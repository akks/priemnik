﻿require_relative 'pkutils'
require 'watir'
require_relative 'pkhead'


addrs=Hash.new
Dir.mkdir('hse') unless Dir.exist?('hse') 

s=''
if File.exist?('hse/hse_specs.htm')
  File.open('hse/hse_specs.htm', 'rb') do |f| 
    s = f.read
  end
else
MaybeHeadless do
  b = Watir::Browser.new :firefox
  b.goto('https://priem2.hse.ru/bachstats.html')
  sleep(5)
  s = b.html
  File.open('hse/hse_specs.htm', 'wb') do |f| 
    f.write(s)
  end
  b.close
  httpToFile('https://ba.hse.ru/kolmest2019', 'hse/hse_kolmest.htm')
end

end
require 'nokogiri'

dt = Time.now.strftime("%Y.%m.%d.%H")
addrs = Array.new
tb = Nokogiri::HTML(s).xpath("//table/tbody")
tb.xpath("./tr").each do |row|
#doc.xpath("//a/@href").each do |href|
    spec=row.xpath("./td[1]/text()").text
	href=row.xpath("./td[2]/a/@href").text
	spec.gsub!(/"/,'')
	next if spec==='' or href===''
	puts "#{spec}: #{href}"
	filename=getFileName('hse',href[/(\d+)\.xls/,1]+spec,dt,'xlsx')
	httpToFile('https://priem2.hse.ru'+href, filename, identity:true)
	convertToCSV(filename, false)
end

