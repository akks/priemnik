require_relative 'pkdb'
require 'set'
require 'nokogiri'

vuz="СПБГУ"
initDBs(vuz)


files = Dir.glob("spbgu/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new

@db.addSpec("?", "СПБГУ", "?", 1, 1)
specID='?'
dbc = @db
for f in files do

dt = getDateFromName(f).gsub(/\-/,'.')

print "File #{f} : #{dt} " 
n=0

mag=false
if f.include?('-1.html')
  dbc=@db
  specID = '?'
  next if dbc.hasDateFlag(specID, vuz, dt)
else
  dbc=@dbm
  mag=true
  specID = f[/\d\d\.\d\d\-(.*)\.html/,1]
  puts "-----#{specID}-----"
  next if dbc.hasDateFlag(specID, vuz, dt)
  dbc.addSpec(specID, vuz, specID, 1, 1)
end


s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

doc = Nokogiri::HTML(s)
tb = doc.xpath("//table/tbody")

ball = 0
num = 1
tb.xpath("./tr").each do |row|
if mag
 num = row.xpath("./td[1]/text()").to_s.strip.to_i
 name = row.xpath("./td[2]/text()").to_s
 orig = ''
else
 num = 1
 name = row.xpath("./td[3]/text()").to_s
 orig = row.xpath("./td[6]/text()").to_s
end
 next if name.nil? or name === ''
# puts name
 name=normal(name)
 fio=toFio(name)

 dbc.addRow(name, fio, orig==="Да", ball.to_i, "СПБГУ", specID, num, 1,0,0,0,dt)
#numOrig+=1 if orig==="оригинал"
 n+=1
end
puts "#{n} заявлений"

end

commitDBs()

