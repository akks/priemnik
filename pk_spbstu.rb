﻿require_relative 'pkutils'
require 'watir'
require_relative 'pkhead'


def saveHtml(txt,spec)

m = /По состоянию на (\d\d)\.(\d\d)\.(\d\d\d\d) (\d+)/.match(txt)
dt=''
dt = "#{m[3]}.#{m[2]}.#{m[1]}.#{m[4]}" unless m.nil?



puts spec
specID = spec[/\d\d\.\d\d\.\d\d(\.\d\d)?/]
return unless specID
spec.gsub!(/\d\d\.\d\d\.\d\d(\.\d\d)?\s*/,'')
spec.gsub!(/\//,'-')
puts specID
#specID+='п'

if spec.length > 100
    spec = spec[0..100]
end

filename = getFileName("spbstu",spec,dt)

puts "#{specID}: #{spec} #{dt}"
puts "Saving html to #{filename}"

File.open(filename, 'wb') {|f| 
f.write "<html><head><title>#{spec}</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write txt[/<table.*<\/table>/m]
f.write "</body></html>\n"
}
end


MaybeHeadless do

Watir.default_timeout = 90


puts "Opening Firefox..."
b = Watir::Browser.new :firefox
b.goto 'http://www.spbstu.ru/abit/admission-campaign/general/abiturients/' 

[ 'Магистратура', 'Бакалавриат/Специалитет'].each do |level|

b.select_list(:name, "vprogram").select(level)
b.select_list(:name, "edform").select('Очная')
b.select_list(:name, "finance").select('Бюджет')


dept_sel = b.select_list(:name, "department")
opts = []
dept_sel.options.each { |dep|
    next if dep.text =~/не выбрано/
    opts.push(dep.text)
}

opts.each { |dep|
	puts dep
	dept_sel = b.select_list(:name, "department")
	dept_sel.select(dep)
	puts 'Selected'+dep
	sleep(0.3)

	group_sel = b.select_list(:name, "group")
	gopts=[]
	group_sel.options.each { |grp|
	    next if grp =~/не выбрано/
	    gopts.push(grp.text)
	}
	puts gopts
	gopts.each { |grp|
		puts "Selecting "+grp
		group_sel.select(grp)
		sleep(0.1)
		puts "Selected "+grp
		b.button(:name => "set_filter").click
		fname = grp
		fname = fname+'-mag' if level.include?('Маг')
		saveHtml(b.html, fname)
	}
}
end
end

