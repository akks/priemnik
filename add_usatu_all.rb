@magistr=false if @magistr.nil?

require_relative 'pkdb'
require_relative 'mod_usatu'
tm0=Time.now

if @magistr
db = PKDB.new('data_all_mag.db')
files = Dir.glob('usatu_mag/*.html')
else
db = PKDB.new('data_all.db')
files = Dir.glob('usatu/*.html')
end

puts 'Checking tables...'
vuz='УГАТУ'
db.createTables()
db.cleanTablesIfNeeded(vuz)

p = UGATUParser.new(@magistr)

puts 'Preparing database...'
db.prepare()

puts 'Processing files...'
for f in files do
  p.readHeaders(f)

  p.findSpec() do |specID, spec, totalPlan, celPlan, dateStr|
    print "File #{f} #{specID}\n" 
    next if db.hasDateFlag(specID, 'УГАТУ', dateStr)
    db.addSpec(specID, 'УГАТУ', spec, totalPlan, celPlan)
    i=1
    p.forAllRecords() do |num,name,ball,b1,b2,b3,orig|
      name=normal(name)
      fio=toFio(name)
      numOr=num
#      puts name
      db.addRow(name, fio, orig, ball, 'УГАТУ', specID, num, i, b1,b2,b3,dateStr)
      i+=1 if orig
    end
  end
end

puts 'Max date: '+db.maxDateVuz('УГАТУ')
db.commit()

puts Time.now - tm0
