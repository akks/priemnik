﻿require_relative 'pkutils'
require_relative 'pkdb'
require 'strscan'
require 'set'

vuz="ЗАЧИСЛЕН-МГТУ"

db = PKDB.new("data_all.db")
db.createTables()
db.cleanTables(vuz)

db.prepare()
files = Dir.glob("mgtu_p/*.txt") 

specs=SortedSet.new


for f in files do

s=IO.read(f)
s.force_encoding('UTF-8')
fixE(s)	   

print "File #{f}\n" 

sc = StringScanner.new(s)


spec=''
specID=''
skip=false

loop do

break if sc.scan_until(/((\d\d\.\d\d\.\d\d)\s*«(.*?)»)|(([А-Я][А-Яа-я\-]+\s[А-Я][А-Яа-я\-]+\s[А-Я][А-Яа-я\-]+);)/).nil?

if sc[1]
#puts "~~~#{sc[1]}~~~"
	specID = sc[2]
	spec = sc[3]
	db.addSpec(specID, vuz, spec, 1, 1)
	puts "Spec: #{specID}: #{spec}"
else
	nm = sc[5]
	ball = 0
next unless nm
	name=normal(nm)
next if name===''
puts name
	fio=toFio(name)
	db.addRow(name, fio, true, ball, vuz, specID, 1,1,0,0,0)
end

break if sc.eos?

end
end


db.commit()
