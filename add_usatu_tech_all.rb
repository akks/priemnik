require_relative 'pkdb'
require 'nokogiri'
tm0=Time.now

db = PKDB.new("data_all.db")
files = Dir.glob("ugatu_tech/*.html") 

print "Checking tables...\n"
db.createTables()
vuz='УГАТУ-СПО'
db.cleanTablesIfNeeded(vuz)

print "Preparing database..\n"
db.prepare()

print "Processing files\n"

for f in files do

dt = getDateFromName(f)
print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)


doc = Nokogiri::HTML(s)

spec = f[/\d\d\.\d\d\.\d\d (.*?)\.html/,1]
specID = f[/\-(\d\d\.\d\d\.\d\d)/,1]

puts "#{specID}:: #{spec}"
next if db.hasDateFlag(specID, vuz, dt)

db.addSpec(specID, vuz, spec, 1, 1)

tb = doc.xpath("//table")

num=1
numOrig=1
tb.xpath("./tr").each do |row|
 name = row.xpath("./td[3]/a/text()").to_s
 ball = row.xpath("./td[4]/text()").to_s.gsub(',','.')
 orig = row.xpath("./td[5]/text()").to_s
 next if name.nil? or name === ''
 puts "#{name} #{ball} #{orig} #{dt}"
 name=normal(name)
 fio=toFio(name)
 db.addRow(name, fio, orig==="Оригинал", ball, vuz, specID, num, numOrig,0,0,0,dt)
 numOrig+=1 if orig==="оригинал"
 num+=1
end

end

puts "Max date: "+db.maxDateVuz(vuz)
db.commit()


puts Time.now-tm0


