﻿require_relative 'pkutils'
require 'watir'
require_relative 'pkhead'

MaybeHeadless do


profile = Selenium::WebDriver::Firefox::Profile.new()#'/home/user/.mozilla/firefox/1o16j5in.default')
b = Watir::Browser.new :firefox, :profile => profile, "acceptInsecureCerts" => true

b.goto 'https://priem.bashedu.ru/list'

require 'nokogiri'

specs=Set.new

b.goto 'https://priem.bashedu.ru/list?el=4&uz=01'

Nokogiri::HTML(b.html).xpath("//select[@id='sp']/option").each { |opt|
#b.select(:id => 'sp').options.each { |opt|
spopt = opt['value']
specs.add(spopt)
}

dt = Time.now.strftime("%Y.%m.%d.%H")
i=0
n=specs.count
for spec in specs 
url = "https://priem.bashedu.ru/list?el=4&uz=01&em=11&sp=#{spec}&dor=3&sof=1&apply=true"
b.goto url

filename = getFileName("bgu",spec,dt)
puts "Saving #{url} to #{filename}, #{100*i/n}%"
File.open(filename, 'wb') {|f| 
  f.write b.html
}
i=i+1
end


end
