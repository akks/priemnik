require 'sqlite3'
require 'sorted_set'
require_relative 'pkutils'

class PKDB
  
  def initialize(file)
     print "Initializing SQLite database at #{file}\n"
     @db = SQLite3::Database.new( file )
     @stamps = nil
  end

  def prepare(opentran=true) 
    @stmt = @db.prepare('insert into records(name,fio,orig,ball,vuz,spec,num,numOr,b1,b2,b3,tm) values ( ?,?,?,?,?,?,?,?,?,?,?,? )') 
    @specStmt = @db.prepare('insert or ignore into specs(spec,vuz,specName,mest,cel) values ( ?,?,?,?,? )') 
    @origsStmt = @db.prepare('insert or ignore into timePlaces(spec,dt,origs) values ( ?,?,? )') 
    @checkStmt = @db.prepare('select 1 from dateflags where specID=? and vuz=? and dt=?') 
    @maxDateStmt = @db.prepare('select max(dt) from dateflags where specID=? and vuz=?') 
    @maxDateTotalStmt = @db.prepare('select max(dt) from dateflags where vuz=?') 
    @setFlagStmt = @db.prepare('insert into dateflags(specID,vuz,dt) values ( ?,?,? )') 

    @db.transaction() if opentran
  end

  def addRow(name,fio,orig,ball,vuz,spec,num,numOr,b1=0,b2=0,b3=0, tm='')
    @stmt.execute name, fio, orig.to_s, ball,  vuz, spec, num, numOr, b1, b2, b3, tm
  end

  def addSpec(id,vuz, name, mest, cel)
    @specStmt.execute id,vuz,name,mest,cel
  end

  def addOrigs(spec, dt, origs)
    @origsStmt.execute spec, dt, origs
  end

  def allSpecDates(vuz)
    stmt = @db.prepare("select specID,dt from dateflags where vuz=?") 
    stamps = Set.new
    stmt.execute(vuz).each { |row|
       stamps.add([[row[0],row[1]]])
    }
    return stamps
    
  end

  def checkExist(specID, vuz, date)
    	res=@checkStmt.execute(specID,vuz,date)
	return res.any?
  end

  def maxDate(specID, vuz)
    	res=@maxDateStmt.execute(specID,vuz)
	return res.first[0]
  end

  def maxDateVuz(vuz)
    	res=@maxDateTotalStmt.execute(vuz)
	return res.first[0]
  end

  def setDateFlag(specID, vuz, date)
    @setFlagStmt.execute specID,vuz,date
  end

  def hasDateFlag(specID, vuz, date)
    @stamps = allSpecDates(vuz) if @stamps.nil?
    if @stamps.include?([[specID,date]]) 
      print "+ "
      return true
    end
    setDateFlag(specID, vuz, date)
    return false
  end
  
  def commit()
  puts "Сохраняем изменения в базе"
  @db.commit()
  @stmt.close()
  @specStmt.close()
  end
  
  def createTables()
    
  sql = <<SQL
      create table if not exists specs (
      vuz varchar(20), 
      spec varchar(100), 
      specName varchar(100),
      mest integer,
      cel integer,
      primary key(vuz,spec)
    ) ;

     create table if not exists dateFlags (
      vuz varchar(20), 
      specID varchar(100), 
      dt string
    );

    create table if not exists records (
      name varchar(100), 
      fio varchar(100), 
      orig boolean,
      ball integer,
      vuz varchar(20),
      spec varchar(100), 
      num integer,
      numOr integer,
      b1 integer,
      b2 integer,
      b3 integer,
	  tm string,
      FOREIGN KEY(vuz, spec) REFERENCES specs(vuz,spec)
    );
  CREATE INDEX if not exists rec_date ON records(tm);
  CREATE INDEX if not exists rec_spec ON records(spec);
  CREATE INDEX if not exists rec_fio ON records(fio);
  CREATE INDEX if not exists rec_name ON records(name);

  create table if not exists dateFlags (
    vuz varchar(20), 
    specID varchar(100), 
    dt string
  );

  create table if not exists timePlaces (
    spec varchar(100), 
    dt string,
    origs integer,
    primary key(spec,dt)
  );
SQL

  @db.execute_batch(sql)
  puts 'Таблицы созданы'
  end
  
  def dropTables()
    
  sql = <<SQL
  drop table if exists records ;
    drop table if exists specs ;
SQL

  @db.execute_batch( sql )
  puts "Таблицы удалены"
  end

  def cleanTablesIfNeeded(vuz)
  if !ARGV[0].nil?
   cleanTables(vuz)
   return true
  end
  return false
  end
  
  def cleanTables(vuz)
  puts "Удаляем все записи для вуза #{vuz}" 
  sql = <<SQL
  delete from specs where vuz = '#{vuz}';
  delete from dateFlags where vuz='#{vuz}';
  delete from records where vuz='#{vuz}';
SQL
  @db.execute_batch( sql )
  end
  
  def execQuery(sql) 
  @db.execute(sql)
  end

  def exec(sql) 
  @db.execute_batch(sql) 
  end
  
  def printSpec(key, spec, vuz, specList)
    rows=execQuery("SELECT name, ball, #{key}, vuz FROM records where spec like '#{spec}' and vuz like '#{vuz}' order by ball desc") 
    for row in rows do
      # печатаем имеющиеся специальности
      puts "%35s; %10s; %s"%[row[0],row[1],specList[row[2]]]
    end
  end
  
  def saveSpec(key, spec, vuz, specList, dt)
    rows=execQuery("SELECT name, ball, #{key}, vuz FROM records where spec like '#{spec}' and vuz like '#{vuz}' and (tm='#{dt}' or tm='') order by ball desc") 
    file = File.open("#{spec}.csv", 'w:cp1251') 
    puts "Сохраняем в файл #{spec}.csv"
    for row in rows do
      # печатаем имеющиеся специальности
      file.puts '%35s; %10s; %s' % [row[0], row[1], specList[row[2]]]
    end
    
    file.close()
  end

def getSpecListByNames(mainvuz, dt)
 specList=Hash.new
soglDates = Hash.new
specs = Hash.new
mesta = Hash.new
specs1 = []
execQuery("SELECT spec,vuz,specName,mest FROM specs").each {|row|
sn = row[2].gsub(/\d\d\.\d\d\.\d\d[^$:]/,'')
#specid = row[2][/\d\d\.\d\d\.\d\d[ИКк]*/]
#sn=sn.gsub(/([А-Яа-я]{2})[А-Яа-я\-]*/,'\1').gsub(/[\d\s\.]/,'')
if row[1] != mainvuz 
  sn="#{row[1]}.#{sn}"
  specs[[row[0],row[1]]]=sn
else
  specs[[row[0],row[1]]]="<a href='#{row[0]}.html'>#{sn}</a>"
  specs1.push(row[0])
  puts "S "+row[0]
end
mesta[[row[0],row[1]]]=row[3]
}

execQuery("SELECT name,spec, max(tm) as max_t, min(tm) as min_t FROM records WHERE vuz='#{mainvuz}' and orig='true' group by name,spec")
	.each do |name,spec,max_t, min_t|
	slist = "cогласие с #{fixDate(min_t)}"
	slist += "по #{fixDate(max_t)}" if max_t!=dt
	soglDates[[name,spec]]=slist
end

 execQuery("SELECT name, ball, vuz, spec, orig, num, numOr FROM records WHERE tm='#{dt}' and vuz='#{mainvuz}' order by name").each do |name,ball,vuz,spec,orig,num,numOr|
	slist = specs[[spec,vuz]]
	slist += " #{num},#{numOr}/#{mesta[[spec,vuz]].to_s}" if mesta[[spec,vuz]]
	slist += "!!" if orig=="true"
	slist += "! "+soglDates[[name,spec]] if orig=="true" and vuz===mainvuz
	slist += "<br/>"
	if specList.key?(name)
		specList[name]+=slist
	else
		specList[name]=slist
	end
end
return specs, specs1, mesta, specList
end

  def getUnos(specs, specList, mesta, dt, mainvuz) 
    unos=Hash.new
    nowOrig=Hash.new

    prefix=''
    ["select distinct(r1.name),r1.ball,r2.vuz,r2.spec,r2.orig,r2.num,r2.numOr,r2.tm
    from records as r1 left join records as r2 on r1.name=r2.name
    where r1.vuz='#{mainvuz}' and r1.tm like '#{dt}' and r2.vuz!='#{mainvuz}' order by r1.name,r2.vuz,r2.spec,r2.tm
    "
    ##and not r2.vuz in('#{@mainvuz}',#{@fio_vuzes})
    #,"select distinct(r1.name),r1.ball,r2.vuz,r2.spec,r2.orig,r2.num,r2.numOr,r2.tm
    #from records as r1 left join records as r2 on r1.fio=r2.fio
    #where r1.vuz='#{@mainvuz}' and r1.tm like '#{dt}' and r2.vuz in(#{@fio_vuzes}) order by r1.name,r2.vuz,r2.spec,r2.tm
    #"
    ].each do |sql|
    
    puts sql
    
    vuzPrev=nil
    specPrev=nil
    namePrev=nil
    ballPrev=nil
    tmPrev=nil
    first_orig_date=nil
    first_date=nil
    last_orig_date=nil
    
    
    execQuery(sql).each do |name,ball,vuz,spec,orig,num,numOr,tm|
      if not (vuz===vuzPrev and spec===specPrev and name===namePrev)
      #Пошла новая строка специальности. В ней будет несколько дат с местами 
        if namePrev
          fin=''
          fin << "согласие c #{fixDate(first_orig_date)}" if first_orig_date
          fin << " по #{fixDate(last_orig_date)}" if last_orig_date and last_orig_date!=tmPrev
          fin << "<br/>"
          specList[namePrev]+=fin
          if first_orig_date
          if first_orig_date===first_date
            nowOrig[namePrev]=specs[[specPrev,vuzPrev]]
            unos[namePrev]="#{ballPrev} </td><td>согласие на</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" if first_orig_date
          else
            unos[namePrev]="#{ballPrev} </td><td>унес согласие</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" 
          end
          end
        end
        slist = prefix+(specs[[spec,vuz]]||'') +' '
        first_orig_date=nil
        last_orig_date=nil
        first_date=nil
      else
        slist = ''
      end
      if mesta[[spec,vuz]]
        mestStr = "#{num},#{numOr}/#{mesta[[spec,vuz]].to_s}:"
           slist << mestStr unless mestStr==='1,1/1:'
      end
      if tm===''
        slist << "?"
      else
        slist << "#{fixDate(tm)}"  # форматируем дату
      end
      if orig=="true"
        slist << "<b>!!</b>"
        first_orig_date=tm if first_orig_date.nil?
        last_orig_date=tm
      end
      first_date=tm if first_date.nil?
    
      slist << ";"
      specList[name] += slist
      tmPrev=tm
      vuzPrev=vuz
      specPrev=spec
      namePrev=name
      ballPrev=ball
    end
    if namePrev
      fin=''
      fin << "согласие c #{fixDate(first_orig_date)}" if first_orig_date
      fin << " по #{fixDate(last_orig_date)}" if last_orig_date and last_orig_date!=tmPrev
      fin << "<br/>"
      specList[namePrev]+=fin 
      if first_orig_date
        if first_orig_date===first_date
          nowOrig[namePrev]=specs[[specPrev,vuzPrev]]
          unos[namePrev]="#{ballPrev} </td><td>согласие на</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" if first_orig_date
        else
          unos[namePrev]="#{ballPrev} </td><td>унес согласие</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" 
        end
      end
    
    end
    prefix='~'
    end
    
    return unos, nowOrig
  end

  def saveSpecTable(fname, vuz)
    f = File.open(fname, 'w') 
    f.write '<html><head><title>Список специальностей</title>'
    f.puts ' <style type="text/css">
   table { 
   border:solid;
   width:100%;
   font-size:100%
   }
   table tr {
     background-color: lightgreen;
     vertical-align: top;
   }
   table tr td:nth-child(2) {
    word-break:break-word;
   }
   table tr td:nth-child(3) {
    word-break:break-word;
   }
   table tr td:nth-child(4) {
    word-break:break-word;
   }
  
   .orig-spec {
     background-color: yellow;
    } 
   .orig-usatu {
     background-color: lime;
    } 
   .orig {
     background-color: khaki;
    } 
    .menu {
        padding: 5px;
        border-collapse:collapse;
        font-size: 120%;
    }
    .menu ul:hover {
        background: yellowgreen;
    }
    .menu a {
        color:#000;
    }
                                
   }

  </style>'
    f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
    f.write "</head><body>"

    dt='?'
    execQuery("select max(dt) from dateFlags where vuz='УГАТУ'").each { |row| dt=row[0].to_s }

    f.write "<ul class='menu'>"
    f.write "<li><a href='updateDates.html'>Даты обновления списков / УГАТУ: #{dt}</a>"
    #f.write "<li><a href='counts2.html'>Количество заявлений с историей, обновление раз в 10 минут</a>"
    f.write "<li><a href='index.html'>Бакалавриат/специалитет</a> 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; <a href='indexm.html'>Магистратура</a>"
    f.write "<li><a href='changes.html'>Изменения за день (Бакалавриат)</a> 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; <a href='changesM.html'>Изменения за день (Магистратура)</a>"
    f.write "<li><a href='changesH.html'>Изменения за час (Бакалавриат)</a> 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; <a href='changesMH.html'>Изменения за час (Магистратура)</a>"
    #f.write "<li><a href='stats.xlsx'>Статистика подачи заявлений</a> 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; 	&nbsp; <a href='stats_magistr.xlsx'>Статистика подачи заявлений в магистратуру</a>"
    f.write "</ul>"

    f.write '<table><thead><tr><th></th><th></th><th></th><th></th><th></th></tr></thead><tbody>'

    bs1 = Hash.new
    bs2 = Hash.new
    bs3 = Hash.new
    os = Hash.new
    sn = Hash.new
    ms = Hash.new
    mv = Hash.new

    
    sql="""select spec, specName,mest
    from specs
    where vuz='#{vuz}'
    """
    execQuery(sql).each { |row|
      ms[row[0]] = row[2]
      sn[row[0]] = row[1]
      puts row[0]+"--"+row[1]
    }
    
    sql="""select records.spec, specName,mest,sum(1) as origs,
    group_concat((case when (numOr<=mest) then ball else NULL end), ', '),
    '',
    group_concat((case when (numOr>mest) then ball else NULL end), ', ')
    from records left join specs on records.vuz=specs.vuz and records.spec=specs.spec
    where records.vuz='#{vuz}' and tm=(select max(dt) from dateFlags where vuz='#{vuz}') and orig='true' 
    group by records.spec"""

    #sql="""select records.spec, specName,mest,sum(1) as origs    from records left join specs on records.vuz=specs.vuz and records.spec=specs.spec
    #group by records.spec"""
    execQuery(sql).each { |row|
      os[row[0]] = row[3]
      bs1[row[0]] = row[4]
      bs2[row[0]] = row[5]
      bs3[row[0]] = row[6]
#      ms[row[0]] = row[2]
#      sn[row[0]] = row[1]
      mv[row[0]] = row[7]

#      puts row[0]+"--"+row[1]
    }
    
    for spec in sn.keys
    #   rows=execQuery("SELECT specName, mest, cel FROM specs where spec like '#{spec}' and vuz like '#{vuz}'")
    #         .first
    #   if rows.nil?
    #   puts "Не найдена специальность #{spec} в базе" 
    # # exit
    #   next
    #   end
      
      specName = sn[spec]
      puts "Добавляем к списку index.html #{spec},==#{specName}=="
      next unless specName
      specName.sub!(/\d\d.\d\d.\d\d\s*/,'')
      mest = ms[spec]
      #mestv = mv[spec]
      origs = os[spec]
      #fname = "#{spec} #{specName}.html".gsub(/\s+/,'-')
      fname=spec+'.html'
      f.puts "<tr><td><a href=\"#{fname}\">#{spec} #{specName}</a></td><td>#{bs1[spec]}</td><td>#{bs2[spec]}</td><td>#{bs3[spec]}</td><td>#{origs}</td><td>#{mest}</td></tr>"
      puts "<tr><td><a href=\"#{fname}\">#{spec} #{specName}</a></td><td>#{bs1[spec]}</td><td>#{bs2[spec]}</td><td>#{bs3[spec]}</td><td>#{origs}</td><td>#{mest}</td></tr>"
      
    end

    f.write "</tbody></table></body></html>"
    f.close
end   
  
  def saveSpecHtml(spec, vuz, specList, dt, unos=nil, nowOrig=nil)
    puts dt

    rows=execQuery("SELECT specName, mest, cel FROM specs where spec like '#{spec}' and vuz like '#{vuz}'").first
    if rows.nil?
    puts"Не найдена специальность --#{spec}-- в базе" 
    return
    end
    specName = rows[0]
    specName.sub!(/\d\d.\d\d.\d\d\s*/,'')
    mest = rows[1]
    cel = rows[2]
    
    q1 = "SELECT count(name) FROM records where spec like '#{spec}' and vuz like '#{vuz}' and orig='true' and (tm='#{dt}' or tm='')"
    orig=execQuery(q1).first()[0].to_i
    
    num=execQuery("SELECT count(name) FROM records where spec like '#{spec}' and vuz like '#{vuz}' and (tm='#{dt}' or tm='')").first()[0].to_i
    
#puts rows
    fname = "#{spec} #{specName}.html".gsub(/\s+/,'-')
    fname=spec+'.html'
    f = File.open(fname, 'w')
    print "Сохраняем в файл #{fname}: "
    
    f.write "<html><head><title>#{spec} #{specName}</title>"
    f.puts ' <style type="text/css">
   table { 
   border:solid; 
   width:100%;
   font-size:100%
   }
   table tr {
     background-color: lightgreen;
     vertical-align: top;
   }
   table tr td a {
	text-decoration: none;
	color:inherit;
   }
   .orig-spec {
     background-color: yellow;
    } 
    .orig-usatu {
      background-color: lime;
     } 
     .orig-double {
      background-color: orange;
     } 
     .orig {
     background-color: khaki;
    } 

  </style>'
    f.puts "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
    f.puts "</head><body>"
    f.puts "<h1>#{spec} #{specName} на #{dt}</h1>"
    f.puts "<h1>всего #{mest} мест, #{cel} целевых</h1>"
    f.puts "<h1>#{num} заявлений, #{orig} согласий</h1>"

    f.puts "<div class='orig-spec'> Есть согласие зачисляться на эту специальность </div>"
    f.puts "<div class='orig-double'> Двойное согласие (УГАТУ+другой вуз) в текущих загруженных списках </div>"
    f.puts "<div class='orig-usatu'> Согласие на зачисление на другую специальность УГАТУ</div>"
    f.puts "<div class='orig'> Согласие в другом вузе</div>"
    f.puts "<div style='background-color: lightgreen;'> Нет согласия в известном системе вузе</div>"
    f.puts "80,20 / 30:07.16.23!! означает \"80-й в списке по копиям и 20-й по согласиям на 30 мест\", \"!!\"=написал согласие<br/> в списке на 16 июля, 23 часа"
    
    f.puts "Студенты-однофамильцы с одинаковыми инициалами могут объединиться в одну ячейку таблицы, у таких избыточно много заявлений.<br/>"

    f.puts "Номера: по О = по согласиям, по К без ОУ = по копиям за вычетом подавших согласие на другое направление УГАТУ,<br/> по К без О = по копиям за вычетом подавших согласия на другие направления всезх вузов<br/><br/>"
    f.puts "поиск - стандартным Ctrl-F<br/>"
    f.puts "<a href=\"#history\">- последние изменения, время обновления списка там же -</a><br/>"
    f.puts "<table><thead><th>№</th><th>по О</th><th>по К<br/>без ОУ</th><th>по К<br/>без О</th><th>ФИО</th><th>Баллы<br/>М+Р+Ф/И</th><th>Куда подавал</th></thead><tbody>"
    re = Regexp.new( "#{specName.gsub('(','\(').gsub(')','\)')}.{0,20}!!!")
    #puts "RE=====#{specName.gsub('(','\(').gsub(')','\)')}.{0,20}!!!"
    ##origs=1
    origs_usatu=0
    origs_other=0
    nams=[]
    rows=execQuery("SELECT name, ball, name, vuz, num, b1,b2,b3,numOr FROM records where spec like '#{spec}' and vuz like '#{vuz}' and (tm='#{dt}' or tm='') order by num") 
    for row in rows do
      # печатаем имеющиеся специальности
      specs = specList[row[2]] ## текст с историей заявлений для данного человека
      if specs.nil?
        puts "Список специальностей #{row[2]} не найден!"
        next
      end
      dost = row[1].to_i-row[5].to_i-row[6].to_i-row[7].to_i
      balls = "#{row[1]} <br/> #{row[5]}+#{row[6]}+#{row[7]}+#{dost}"
      origs = row[8].to_i
      
      if specs=~ re
        if nowOrig[row[0]].nil? 
          cls = 'class="orig-spec"'
        else
          puts "Двойное согласиел **** #{nowOrig[row[0]]}"
          cls = 'class="orig-double"'
        end
        f.puts "<tr %s><td><a name='#%s'></a>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>\n<td>%s</td><td valign=\"top\">%s</td></tr>"%[cls,row[0],row[4],origs,row[4]-origs_usatu,row[4]-origs_usatu-origs_other,row[0],balls,specs]
        ##origs+=1
      else 
        if specs=~ /.*!!!.*/
          cls = 'class="orig-usatu"' 
          f.puts "<tr %s><td><a name='%s'></a>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>\n<td>%s</td><td valign=\"top\">%s</td></tr>"%[cls,row[0],row[4],origs,row[4]-origs_usatu,row[4]-origs_usatu-origs_other,row[0],balls,specs]
          origs_usatu+=1
        else
          if specs=~ /.*!!.*/
            cls = 'class="orig"' 
            f.puts "<tr %s><td><a name='%s'></a>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>\n<td>%s</td><td valign=\"top\">%s</td></tr>"%[cls,row[0],row[4],origs,row[4]-origs_usatu,row[4]-origs_usatu-origs_other,row[0],balls,specs]
            origs_other+=1
          else
            cls = ''
            f.puts "<tr %s><td><a name='%s'></a>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>\n<td>%s</td><td valign=\"top\">%s</td></tr>"%[cls,row[0],row[4],origs,row[4]-origs_usatu,row[4]-origs_usatu-origs_other,row[0],balls,specs]
          end
        end
      end
#      spectxt = specs.gsub(/([>!\d;])([ ?А-Яа-я~])/,'\1<br/>\2')
      nams << "#{row[2]}"
    end
    f.puts "</table>"
    f.puts "</table>"
    saveDiffs(spec, f, vuz)
    f.puts "<div id=\"history\"/>"
    f.puts("<table><tbody>")
    nams.each do |k|
      f.puts "<tr><td>#{k}</td> <td>#{unos[k]}</td></tr>" if unos.key?(k)
    end
    f.puts("</table></tbody>")
    f.puts "</body></html>\n"
    
    f.close()
  end


def saveDiffs(specCode, ff, vuz)
prikl=false
if specCode.include?('п')
  specCode[/п/]=''
  prikl=true
end

#if specCode.include?('.04.')
#db = PKDB.new("data_all_mag.db")
#else
#db = PKDB.new("data_all.db")
#end


lists=Hash.new
olists=Hash.new
balls={}
dates=SortedSet.new
specs=SortedSet.new
counts=Hash.new
countsO=Hash.new
info=Hash.new

sql = "select spec, specName, mest,cel from specs where vuz='#{vuz}' and spec='#{specCode}'"
sql2 = "select num,name,ball,b1,b2,b3,orig,tm from records where vuz='#{vuz}' and spec='#{specCode}' order by tm asc"

row = execQuery(sql).first

spec=row[0]
specName=row[1]
totalPlan=row[2]
celPlan=row[3]

puts "#{spec}, #{specName}, #{totalPlan}, #{celPlan}"

specs.add(spec)
		
totalNum = 0
origNum = 0
fios=SortedSet.new
ofios=SortedSet.new
fballs={}

prevDate=nil

execQuery(sql2).each { |num,name,ball,b1,b2,b3,orig,dateStr|
		
	
	if !prevDate.nil? and prevDate!=dateStr
		dates.add(prevDate)
#		puts dateStr
#		puts dateStr, fios.to_a
        balls[prevDate]=fballs
		lists[prevDate]=fios
		olists[prevDate]=ofios
		counts[prevDate]=totalNum
		countsO[prevDate]=origNum
		totalNum = 0
		origNum = 0
		fios=SortedSet.new
		fballs={}
		ofios=SortedSet.new
	end
	prevDate=dateStr

	totalNum += 1
	origNum += 1 if orig==='true'
	fio=normal(name)
	info[fio]=ball
	
	if !(fio==='ФИО')
#			puts num,fio
        fballs[fio] = ball
		fios.add(fio)
		ofios.add(fio) if orig==='true'
	end
	
#	puts "#{dateStr} #{spec} #{totalNum}"
}
dates.add(prevDate)
lists[prevDate]=fios
olists[prevDate]=ofios
balls[prevDate]=fballs

counts[prevDate]=totalNum
countsO[prevDate]=origNum

prevDate=nil
ff.puts '<h2>История изменений: </h2><table><tbody>'
dates.each do |date|
  list2=lists[date]
  olist2=olists[date]
  balls2=balls[date]
  if prevDate 
    balls1=balls[prevDate]
    list1=lists[prevDate]
    olist1=olists[prevDate]
  else
    balls1={}
    list1=SortedSet.new    
    olist1=SortedSet.new    
  end

  ff.puts "<tr><td colspan='2'>#{date} #{counts[date]} заявлений, #{countsO[date]} согласий</td>"
#  puts date
  balldiff=''
  balls2.each do |fio, ball| 
#    print "#{fio}:#{ball}"
    ball_old = balls1[fio]
    if !(ball_old.nil?) and (ball != ball_old)
       balldiff += "#{fio}: #{ball_old}->#{ball}, "
    end
  end
#  puts "----"

  diff1 = (list2-list1)
  diff2 = (list1-list2)
  odiff1 = (olist2-olist1)
  odiff2 = (olist1-olist2)
  xrefer = Proc.new {|x| "<a href='\##{x}'>#{x}(#{info[x]})</a>" }

  ff.puts "<tr><td>+ </td><td>"+diff1.to_a.map(&xrefer).join(', ')+'</td></tr>'  unless diff1.empty?
  ff.puts "<tr><td>- </td><td> "+diff2.to_a.map(&xrefer).join(', ')+'</td></tr>' unless diff2.empty?
  ff.puts "<tr><td>+О</td><td> "+odiff1.to_a.map(&xrefer).join(', ')+'</td></tr>' unless odiff1.empty?
  ff.puts "<tr><td>-О</td><td>"+odiff2.to_a.map(&xrefer).join(', ')+'</td></tr>' unless odiff2.empty?
  ff.puts "<tr><td>Б</td><td>"+balldiff+'</td></tr>' unless balldiff.empty?

  prevDate=date

end
ff.puts '</tbody></table>'

end

end

@db = nil
@dbm = nil

def initDBs(vuz)
  @db = PKDB.new("data_all.db")
  @dbm = PKDB.new("data_all_mag.db")
  @db.createTables()
  @dbm.createTables()
  @db.cleanTablesIfNeeded(vuz)
  @dbm.cleanTablesIfNeeded(vuz)
  @db.prepare()
  @dbm.prepare()
end

def commitDBs()
  @db.commit()
  @dbm.commit()
end



