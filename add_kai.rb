require_relative 'pkdb'
require 'nokogiri'
require 'set'


db = PKDB.new("data_all.db")
db.createTables()

vuz="КАИ"
db.cleanTablesIfNeeded(vuz)




files = Dir.glob("kai/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new

db.prepare()

for f in files do

dt = getDateFromName(f)
print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)


doc = Nokogiri::HTML(s)

spec = doc.xpath("/html/head/title/text()").text
next if spec===''
specID = f[/\-(\d\d\.\d\d\.\d\d)/,1]

while specs.include?(specID)
specID+="'"
end

next if db.hasDateFlag(specID, vuz, dt)

db.addSpec(specID, vuz, spec, 1, 1)

tb = doc.xpath("//table/tbody")

ball = 0
num=0
tb.xpath("./tr").each do |row|
 name = row.xpath("./td[2]/text()").to_s
 orig = row.xpath("./td[4]/text()").to_s
 next if name.nil? or name === ''
# puts name
 name=normal(name)
 fio=toFio(name)

 db.addRow(name, fio, orig==="Оригинал", ball, vuz, specID, 1, 1,0,0,0,dt)
#numOrig+=1 if orig==="оригинал"
num+=1
end
puts "#{num} заявлений"

end

db.commit()




