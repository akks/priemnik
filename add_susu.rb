require_relative 'pkdb'
require 'set'
require 'nokogiri'

db = PKDB.new("data_all.db")
db.createTables()
db.cleanTablesIfNeeded(vuz="ЮУРГУ")

db.prepare()

files = Dir.glob("susu/*.html") 
  
specs=SortedSet.new

plans=Hash.new
cels=Hash.new

for f in files do

dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

#if s.include?('Нет данных')
#   puts('Нет данных')
#   next
#end

doc = Nokogiri::HTML(s)
s1 = doc.xpath("//table/tbody")
if s1.nil?
   puts('Нет таблицы!')
   next
end

#   <p>Число мест: общее — 30,
#      квота — 0, целевой набор — 0.</p>

spec=s[/(\d\d\.\d\d\.\d\d.*?)\.</m,1]
specID = spec[/\d\d\.\d\d\.\d\d/]
specName=spec

plan = s[/Число мест: общее.*?(\d+)/,1]
cel = s[/целевой набор.*?(\d+)/,1]

#next   if plan.nil? or cel.nil?


db.addSpec(specID, vuz, specName, plan.to_i, cel.to_i)
next if db.hasDateFlag(specID, vuz, dt)

i=1
n=0
s1.xpath("./tr").each do |row|
     #if row.xpath("./td[1]/@rowspan").to_s === ''
     num = row.xpath("./td[1]/text()").to_s
     name = row.xpath("./td[4]/text()").to_s
     orig =  row.xpath("./td[5]/text()").to_s.include?('согласие')
     ball = row.xpath("./td[6]/text()").to_s.to_i
     b1,b2,b3 = row.xpath("./td[9]/span/text()")
     name=normal(name)
     next if name.empty?
     fio=toFio(name)
     db.addRow(name, fio, orig, ball, vuz, specID, num, i, b1.to_s.to_i, b2.to_s.to_i, b3.to_s.to_i, dt)
     i=i+1 if orig
     n=n+1
end

puts "#{specID}--#{specName} #{plan}/#{cel}, #{n} заявлений"
end

db.commit()
