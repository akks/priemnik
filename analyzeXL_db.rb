﻿require_relative 'pkdb'
require 'rubyXL'

@magistr=false if @magistr.nil?
if @magistr
db = PKDB.new("data_all_mag.db")
else
db = PKDB.new("data_all.db")
end

db.prepare()

wb = RubyXL::Workbook.new

wb.add_worksheet('Stats')
wb.add_worksheet('StatsO')
wb.add_worksheet('Konkurs')
wb.add_worksheet('KonkursO')
wb.add_worksheet('Prohod')
wb.add_worksheet('Sosedi')

prohod = wb['Prohod']
stats = wb['Stats']
konkurs =  wb['Konkurs']
statsO =  wb['StatsO']
konkursO =  wb['KonkursO']
sosedi =  wb['Sosedi']
wb.worksheets.delete_at(0)

counts=Hash.new
konkursVals=Hash.new
countsO=Hash.new
konkursValsO=Hash.new

dates=SortedSet.new
specs=SortedSet.new

plans=Hash.new
cels=Hash.new

specFullName=Hash.new

total=Hash.new
totalO=Hash.new

totalGr=Hash.new
totalGrO=Hash.new

sql = "select spec, specName, mest,cel from specs where vuz='УГАТУ'"
db.execQuery(sql).each { |specID, spec, totalPlan, celPlan|
	specs.add(specID)
	specFullName[specID]=spec
	plans[specID] = totalPlan
	cels[specID] = celPlan
	puts spec
}


sql = "select specName,mest,sum(1) as origs,sum(ball*(numOr=mest)) as P2,sum(ball*(numOr=specs.mestv)) as P1 from records left join specs on records.vuz=specs.vuz and records.spec=specs.spec
where records.vuz='УГАТУ' and tm=(select max(dt) from dateFlags where vuz='УГАТУ') and ball>0 and numOr<=mest and orig='true'
group by records.spec"

i=0
db.execQuery(sql).each { |row|
 j=0
 for v in row
   prohod.add_cell(i,j,v)
   j+=1
 end
 i+=1
}
prohod.change_column_width(0,30)


sql = "select tm,spec,sum(1) as count, sum(orig='true') as countO from records  where records.vuz='УГАТУ' group by tm, spec order by spec asc,tm asc"

db.execQuery(sql).each { |tm,spec,totalNum,origNum|
#	puts "#{tm} #{spec} #{totalNum} #{origNum}"
	dates.add(tm)
	totalPlan = plans[spec]

	counts[[tm,spec]]=totalNum
	konkursVals[[tm,spec]]='%.2f'% (1.0*totalNum/totalPlan)

	countsO[[tm,spec]]=origNum
	konkursValsO[[tm,spec]]='%.2f'% (1.0*origNum/totalPlan)
}

i=0

[stats, statsO, konkurs, konkursO].each{ |ws|
	ws.add_cell(i,0,"специальность")
	ws.add_cell(i,1,"м")
	ws.add_cell(i,2,"ц")
j=2
dates.each { |date|
#	stats.write ";#{date}"
	date2 =date.gsub(/^\d+\.\d(\d+)\.(\d+) (\d+)$/,'\2,\3')

	ws.add_cell(i,j,"#{date2}")
	ws.change_column_width(j,4)
	j+=1     
	total[date] = 0
	totalO[date] = 0
}
}

if @magistr
groups={'ОНФ'=>['01.04.02'], 'ФИРТ'=>['02.03.03','09.03.01','09.03.02','09.03.03','09.03.04','09.05.01','10.03.01','10.05.05']}
else
groups={'ОНФ'=>['01.03.02','01.03.04','02.03.01'],
        'ФИРТ-И'=>['02.03.03','09.03.01','09.03.02','09.03.03','09.03.04','09.05.01','10.03.01','10.05.05'],
        'ФИРТ-Ф'=>['27.03.02','27.03.03','27.03.04','27.05.01'],
        'АВИЭТ'=>['11.03.02','11.03.04','12.03.01','12.03.04','13.03.02','11.05.04','13.05.02'],
        'ФАДЭТ'=>['13.03.01','13.03.03','23.03.01','24.03.04','24.03.05','25.03.01','24.05.02','24.05.06'],
	'ФАТС'=>['15.03.01','15.03.02','15.03.04','15.03.05','15.03.06','22.03.01','27.03.01','28.03.02','15.05.01'],
        'ФЗЧС'=>['20.03.01','20.05.01'],
        'ИНЭК-Ф'=>['27.03.05']}
end

puts "Saving history..."

groups.values.each { |v|
 dates.each { |date|
   totalGr[[v,date]] = 0
   totalGrO[[v,date]] = 0
 }
}

specs.each { |spec|
	i+=1
	[stats, statsO, konkurs, konkursO].each{ |ws|
	ws.change_column_width(0,30)

	j=0
	ws.add_cell(i,j,"#{specFullName[spec]}")
	j+=1
	ws.add_cell(i,j,plans[spec])
	j+=1
	ws.add_cell(i,j,cels[spec])
	}
	j=2	
	dates.each { |date|
		v = counts[[date,spec]]
		v='0' if v.nil? 
		stats.add_cell(i,j,v.to_i)

		total[date] += v.to_i
	
		v = konkursVals[[date,spec]]
		v='0.0' if v.nil? 
		v['.']=','
		konkurs.add_cell(i,j,v)
		
		v = countsO[[date,spec]]
		v='0' if v.nil? 
		totalO[date] += v.to_i
                statsO.add_cell(i,j,v.to_i)
		
		v = konkursValsO[[date,spec]]
		v='0.0' if v.nil? 
		v['.']=','
		konkursO.add_cell(i,j,v)
		j+=1
	}
}
j=2
i+=1
puts "Saving totals..."

dates.each { |date|
        stats.add_cell(i,j,total[date].to_i)
        statsO.add_cell(i,j,totalO[date].to_i)
	j+=1
}
puts "Saving group totals..."
groups.each { |gr,spcs|
j=1
i+=1
stats.add_cell(i,j,gr)
statsO.add_cell(i,j,gr)
j+=1

dates.each { |date|
 totalGr[[date,gr]] = 0
 totalGrO[[date,gr]] = 0
 spcs.each { |specID|
   totalGr[[date,gr]] += counts[[date,specID]].to_i
   totalGrO[[date,gr]] += countsO[[date,specID]].to_i
 }
 stats.add_cell(i,j,totalGr[[date,gr]].to_i)
 statsO.add_cell(i,j,totalGrO[[date,gr]].to_i)
 j+=1
}
}

puts "Counting other universities..."

sqlO="""select specName,r2.vuz,count(distinct r1.fio) as skolko, group_concat(distinct r1.fio) as fios
from records as r1 left join records as r2 on r1.fio=r2.fio left join specs on r1.spec=specs.spec and r1.vuz=specs.vuz
where r1.tm=(select max(tm) from records where vuz='УГАТУ') and r1.vuz='УГАТУ' and r2.vuz in('УГНТУ','БГУ') 
group by r1.spec,r2.vuz 
union
select specName,r2.vuz,count(distinct r1.name) as skolko,group_concat(distinct r1.fio) as fios
from records as r1 left join records as r2 on r1.name=r2.name left join specs on r1.spec=specs.spec and r1.vuz=specs.vuz
where r1.tm=(select max(tm) from records where vuz='УГАТУ') and r1.vuz='УГАТУ' and not r2.vuz in('УГАТУ','УГНТУ','БГУ') 
group by r1.spec,r2.vuz 
order by specName
"""

i=0
db.execQuery(sqlO).each { |row|
 j=0
 for v in row
   sosedi.add_cell(i,j,v)
   j+=1
 end
 i+=1
}
sosedi.change_column_width(0,30)

puts "Saving stats.xlsx..."
if @magistr
wb.save("stats_magistr.xlsx")
else
wb.save("stats.xlsx")
end