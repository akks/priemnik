require_relative 'pkdb'

Dir.mkdir('ugntu') unless Dir.exist?('ugntu') 

# httpToFile('http://pk.rusoil.net/page/plan-priema-na-programmy-bakalavriata-i-specialiteta-na-2017-18-uchebnyy-god','ugntu/plan.htm') if not File.exist?('ugntu/plan.htm')
# httpToFile('http://pk.rusoil.net/page/plan-priema-na-programmy-magistratury-na-2017-18-uchebnyy-god','ugntu/planm.htm') if not File.exist?('ugntu/planm.htm')
httpToFile('http://pk.rusoil.net/page/plan-priema-na-programmy-bakalavriata-i-specialiteta','ugntu/plan.htm') if not File.exist?('ugntu/plan.htm')
httpToFile('http://pk.rusoil.net/page/plan-priema-na-programmy-magistratury','ugntu/planm.htm') if not File.exist?('ugntu/planm.htm')

allspecs = {}
mainUrl = "https://ams.rusoil.net/abitonline/onlinerating.php"
headers = {"Host"=>"ams.rusoil.net","Origin"=>"https://ams.rusoil.net","Referer"=>mainUrl}

s = httpToFile(mainUrl,
    'ugntu/bakalavr.htm',
    post:true,
    headers:headers,
    params:{"filial"=>"1","fob"=>"1","kvalif"=>"4"}
)

#<td><a href="onlinepeople_list.php?id=18" target="_blank">
#21.03.01 Эксплуатация и обслуживание объектов добычи газа, газоконденсата и подземных хранилищ (БГГ)</a>

allspecs["Бакалавриат"]=s.scan(/<a href="onlinepeople.php\?id=(\d+).*?>[\s\n]*(.*?)<\/a>/m).to_a

s = httpToFile(mainUrl,
    'ugntu/specialist.htm',
    post:true,
    headers:headers,
    params:{"filial"=>"1","fob"=>"1","kvalif"=>"2"}
)
allspecs["Специалитет"]=s.scan(/<a href="onlinepeople.php\?id=(\d+).*?>[\s\n]*(.*?)<\/a>/m).to_a


s = httpToFile(mainUrl,
    'ugntu/magistr.htm',
    post:true,
    headers:headers,
    params:{"filial"=>"1","fob"=>"1","kvalif"=>"1"}
)
allspecs["Магистратура"]=s.scan(/<a href="onlinepeople.php\?id=(\d+).*?>[\s\n]*(.*?)<\/a>/m).to_a

puts allspecs
              
allspecs.each do |level, specs|


dt = Time.now.strftime("%Y.%m.%d.%H")

for spec in specs do
addr ='https://ams.rusoil.net/abitonline/onlinepeople.php?id='+spec[0].to_s
s = httpToString(addr)
fixE(s)
s1 = /<table border.*?<\/table>/m.match(s)
## <h2 align="center"><b>Список поступающих:БГБ Бурение нефтяных и газовых скважин (г. Уфа)</b></h2>
s2 = spec[1]
#/Список лиц подавших документы: (.*?)<\/b>/m.match(s)
next if s1.nil? or s2.nil?
specName=s2
if specName.length > 116
    specName = specName[0..100]+specName[-15..-1]
end


origNum = s1[0].scan(/>Оригинал</).size()
copyNum = s1[0].scan(/>Копия</).size() 
totalNum = origNum+copyNum

puts specName + " "+origNum.to_s+'/'+totalNum.to_s

fname = getFileName("ugntu",specName,dt)
#File.open('ugntu/УГНТУ-'+dt+'-'+specName+'-'+level+'.html', 'w') {|f| 
File.open(fname, 'w') {|f| 
f.write "<html><head><title>#{specName[1]}</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write "<h4> #{dt}: всего #{totalNum.to_s}, оригиналов #{origNum.to_s} копий #{copyNum.to_s}</h4>"
f.write s2
f.write "\n"
f.write s1[0]
f.write "</body></html>\n"

}

end

end
