﻿require_relative 'pkutils'
require 'watir'
require 'headless'


def saveHtml(txt,spec)

m = /(\d\d)\.(\d\d)\.(\d\d\d\d)/.match(txt)
#dt=''
#dt = "#{m[3]}.#{m[2]}.#{m[1]}" unless m.nil?

dt = Time.now.strftime("%Y.%m.%d.%H")

filename = getFileName("kai",spec,dt)

specID = spec[/\d\d\.\d\d\.\d\d/]
spec.gsub!(/\d\d\.\d\d\.\d\d\s*/,'')

puts "#{specID}: #{spec} #{dt}"
puts "Saving html to #{filename}"

File.open(filename, 'wb') {|f| 
f.write "<html><head><title>#{spec}</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write txt[/<table width.*<\/table>/m]
f.write "</body></html>\n"
}
end

Headless.ly do
b = Watir::Browser.new :firefox
b.goto 'https://lk.kai.ru/?spisok_new' 
b.select_list(:name, "filial").select_value('0')
#b.select_list(:name, "level").select_value('1')
#b.select_list(:name, "finance").select_value('1')

inst_sel = b.select_list(:name, "inst")
level_sel= b.select_list(:name, "level")
spec_sel = b.select_list(:name, "spec")
forma_sel = b.select_list(:name, "forma")
vid_sel = b.select_list(:name, "vid")
inst_sel.options.each { |inst|
	next if inst.text =~/Институт\/факультет/
	puts inst.text
	inst_sel.select(inst.text)
	level_sel.select('Программа бакалавриата')
	sleep(0.5)

	spec_sel.options.each { |spc|
		next if spc.text =~/специальность/
		spec_sel.select(spc.text)
		    sleep(0.5)
		puts spc.text
		begin
		next if not forma_sel.options.map(&:text).include?('Очная')
		forma_sel.select('Очная')
		    sleep(0.5)
		next if not vid_sel.options.map(&:text).include?('Бюджет')
		vid_sel.select('Бюджет')
		    sleep(0.5)
		sleep(0.5)
		while not b.html.include?('План при')
		    print '..'
		    sleep(0.5)
		end
		
		puts "Saving html #{spc.text}"
		saveHtml(b.html, spc.text)
		rescue
			puts "Нет очной формы/бюджета?"
		end

	}
}
end
