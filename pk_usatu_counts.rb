require 'nokogiri'
require_relative 'pkdb'

@mag=false if @mag.nil?

@token = 'UpbXmlqGcZzi8tJdHkd7B170G2bLQcm4h66vvE2fi8psr7OwalhlHUQZOqcHY3LS'

@mainUrl='https://ugatu.su/abitur/bachelor-and-specialist/admission-statistics/?institution=%D0%A3%D0%93%D0%90%D0%A2%D0%A3&funding=%D0%91%D1%8E%D0%B4%D0%B6%D0%B5%D1%82%D0%BD%D0%B0%D1%8F+%D0%BE%D1%81%D0%BD%D0%BE%D0%B2%D0%B0&education_level=&education_form=%D0%9E%D1%87%D0%BD%D0%B0%D1%8F'

db=PKDB.new('places.db')
db.createTables()
db.prepare()

if true
  hdrs={ 'Upgrade-Insecure-Requests' => '1',
    'Accept' => 'text/html',
    'Cache-Control' => 'max-age=0',
    'Cookie' => 'csrftoken='+@token,
    'Origin' => 'https://ugatu.su',
    'Referer' => @mainUrl,
  }
  
  path = 'counts.html'
  if false #File.exist?(path)
    f = File.open(path)
    q= IO.read(f)
    q.force_encoding('UTF-8')
  else
    q = httpToFile(@mainUrl, path, post:false, headers:hdrs)
  end



  doc = Nokogiri::HTML(q)
  
  dt = Time.now.strftime("%Y.%m.%d.%H")
 
 s = doc.xpath('//table/tbody/tr')
  s.each do |row|
    s = row.xpath("./td[2]/a/text()").to_s.strip
    o = row.xpath("./td[4]/text()").to_s.to_i
    z = row.xpath("./td[5]/text()").to_s.to_i
#    puts "#{s} #{o} #{z}"
#    s[/(.*?)\s/,1]
    db.addOrigs(s,dt,o+z)
    #puts [s,c[0],l[0],o[0]].to_s
  end

  db.commit()
end


fname="counts2.html"

f = File.open(fname, 'w')
print "Сохраняем в файл #{fname}: "

f.write "<html><head><title>Количество заявлений (история), обновление ращз в 10 минут</title>"

f.puts """
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<style type='text/css'>
table { 
  border:solid; 
  width:100%;
  font-size:100%
  }
  table tr {
    background-color: lightgreen;
    vertical-align: top;
  }
  table tr td:n-th-child(4) {
    word-break:break-word;
  }
  </style>
  </head>
  <body>
  Количества по состоянию на #{dt}
  <table>
"""


db1 = PKDB.new('data_all.db')
db1.createTables()
db1.prepare(false)

specs = Hash.new
mesta = Hash.new

db1.execQuery("SELECT spec,specName,mest FROM specs where vuz='УГАТУ'").each {|row|
  sn = "#{row[0]} #{row[1]}" #.gsub(/\d\d\.\d\d\.\d\d[^$:]/,'')
  specs[sn]="<a href='#{row[0]}.html'>#{sn}</a>"
  mesta[sn]=row[2]
}

puts mesta

puts specs


groups = db.execQuery("SELECT spec,dt, origs FROM timePlaces order by spec, dt").group_by {|r| r[0]}

groups.each do |key,vals|
  next if key.nil? || key==''
  op = 0
#  puts "-----#{key}-----"
  n = vals.count
  k=0
  list=''
  omax=0
  vals.each do |v|
    k=k+1
    o =  v[2].to_i
    omax=o
    if op!=o 
      list+=', ' if list!=''
      list+= "#{v[1][8..]}: <b>#{o}</b>"
      op=o
    end
  end


  f.puts "<tr><td>#{specs[key]}</td><td>#{mesta[key]}</td><td><b>#{omax}</b></td><td>#{list}</td></tr>"
end

f.puts """
  </table>
  </body>
  </html>
"""
