require_relative 'pkdb'
require 'nokogiri'
require 'set'

db = PKDB.new("data_all.db")

db.createTables()
vuz="УГАТУ"
db.cleanTablesIfNeeded(vuz)

files = Dir.glob("usatu/*.html") 

db.prepare()

for f in files do
dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)


spec = s[/<h4 class="centered-h">([^<]*), УГАТУ/]

#puts spec
specID = spec[/\d\d\.\d\d\.\d\d/]
specName = spec[/\d\d\.\d\d\.\d\d (.*), УГАТУ/,1]

cel=0
puts "#{specID} #{specName}"

next if db.hasDateFlag(specID, vuz, dt) or s.length<20000

doc = Nokogiri::HTML(s)

headers = doc.xpath("//div[@class='table-info-label']")
tb = doc.xpath("//table/tbody")

tabidx = -1

for h in headers do
tabidx += 1

title = h.xpath("./h4/text()").text().strip()
info = h.xpath("./div[@class='info']/text()").text().strip()
#puts "#{tabidx} ----"

next unless title.to_s.include?('Общая') 
#puts title, info

specID = spec[/\d\d\.\d\d\.\d\d/]
specID+="-#{tabidx}"
specName = title[/\d\d\.\d\d\.\d\d (.*), УГАТУ/,1]

m_osn=h.xpath("./div[@class='info']/text()").text().strip().match(/Всего мест: (\d+).*?(\d+)/)
puts  "#{specID} #{specName} #{m_osn[1].to_i}"



#m_os=headers[0].xpath("./div[@class='info']/text()").text().strip().match(/Всего мест: (\d+).*?(\d+)/)
#m_cel=headers[1].xpath("./div[@class='info']/text()").text().strip().match(/Всего мест: (\d+).*?(\d+)/)
#m_osn=headers[2].xpath("./div[@class='info']/text()").text().strip().match(/Всего мест: (\d+).*?(\d+)/)
#puts #{m_os[1]},#{m_cel[1]},#{m_osn[1])}


db.addSpec(specID, vuz, specName, m_osn[1].to_i,0) #if f.include?('Общий')

num=1
numOrig=1
ballforplace =  Array.new(1000,1000)
flag=false
#for i in [0,1,2,3] do
next unless tb[tabidx]
tb[tabidx].xpath("./tr").each do |row|
# puts row
 name = row.xpath("./td[3]/text()").to_s
 next if name.empty?
 ball = row.xpath("./td[4]/text()").to_s.to_i
 b1 = row.xpath("./td[5]/text()").to_s.to_i
 b2 = row.xpath("./td[6]/text()").to_s.to_i
 b3 = row.xpath("./td[7]/text()").to_s.to_i
 org = row.xpath("./td[9]/text()").to_s
 if org=~/.*Да.*/ 
   orig=true
 else
   orig=false
 end
# puts "#{name} #{ball} #{orig}"
 next if name.nil? or name === ''
 name=normal(name)
 fio=toFio(name)
 if not tb[tabidx+1]
  if orig
    ballforplace[numOrig] = ball
 #   puts "orig #{ball} ==> #{numOrig}"

  else
    if !flag
      ballforplace[numOrig..]=-1
      #puts ballforplace.to_s
      flag=true
    end
    numOrig = ballforplace.bsearch_index { |x| x<ball } 
 #   puts "#{ball} ==> #{numOrig}"
  end
 end 
 db.addRow(name, fio, orig, ball, vuz, specID, num, numOrig,b1,b2,b3,dt)
 numOrig+=1 if orig
 num+=1
#end
end

end


puts "#{num-1} заявлений в #{f}"
end

db.commit()




