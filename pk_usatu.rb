require 'nokogiri'
require 'uri'
require_relative 'pkutils'

@mag=false if @mag.nil?

@token = 'UpbXmlqGcZzi8tJdHkd7B170G2bLQcm4h66vvE2fi8psr7OwalhlHUQZOqcHY3LS'

if @mag 
@mainUrl='https://ugatu.su/abitur/master/admission-ratings/?'
@dir='usatu_mag'
else
@mainUrl='https://ugatu.su/abitur/bachelor-and-specialist/admission-ratings/?'
@dir='usatu'
end
Dir.mkdir(@dir) unless Dir.exist?(@dir) 

allspecs={}

def findSpecs(allspec, prefix, unit='УГАТУ', osn='Бюджетная основа', edform='Очная', edlevel='Бакалавриат')
  allspec[prefix]=[] unless allspec[prefix]
  
  puts 'Getting specs list'
  
  path = "#{@dir}/specs_#{unit}_#{edform}_#{edlevel}_#{osn}.htm"

  if File.exist?(path)
    f = File.open(path)
    q= IO.read(f)
    q.force_encoding('UTF-8')
  #	fixE(s)
  else
    q="institution=#{unit}&funding=#{osn}&education_level=#{edlevel}&education_form=#{edform}"
    print(q)
    fe=lambda { |x| URI.encode_www_form_component(x) }
    q="institution=#{fe.call(unit)}&funding=#{fe.call(osn)}&education_level=#{fe.call(edlevel)}&education_form=#{fe.call(edform)}"
    

    parameters = {
      'csrfmiddlewaretoken' => @token
    }

    hdrs={ 'Upgrade-Insecure-Requests' => '1',
      'Accept' => 'text/html',
      'Cache-Control' => 'max-age=0',
      'Cookie' => 'csrftoken='+@token,
      'Origin' => 'https://ugatu.su',
      'Referer' => @mainUrl,
    }
  
    q = httpToFile(@mainUrl+q, path, post:false, params:parameters, headers:hdrs)
  end

  doc = Nokogiri::HTML(q)

  s = doc.xpath('//select[@name="specialty"]/option')
  s.each do |v|
    id = v.attr('value')
    next if id.nil? or id==''
    puts id

    spec = v.content
    if unit!='УГАТУ'
      spec += unit 
      #[' - Ишимбай', ' - Кумертау'][unit-2]
  #    id += ['И','К'][unit-1]
    end
    allspec[prefix].push([id,spec,unit,edform,edlevel,osn])
  end

end

if @mag
  findSpecs(allspecs, 'М', 'УГАТУ','Бюджетная основа','Очная','Магистратура')
else
  findSpecs(allspecs, 'B')
  #findSpecs(allspecs, 'S', 'УГАТУ','Бюджетная основа','Очная','Специалитет')
end
puts allspecs
puts 'finished specs list'

allspecs.each do |prefix, specs|
puts prefix
for id,spec,unit,edform,edlevel,osn in specs do
puts id,spec
parameters = {
  'csrfmiddlewaretoken' => @token
}

hdrs={ 'Upgrade-Insecure-Requests' => '1',
  'Accept' => 'text/html',
  'Cache-Control' => 'max-age=0',
  'Cookie' => 'csrftoken='+@token,
  'Origin' => 'https://ugatu.su',
  'Referer' => @mainUrl,
}

q="institution=#{unit}&funding=#{osn}&education_level=#{edlevel}&education_form=#{edform}&specialty=#{id}"
print(q)

fe=lambda { |x| URI.encode_www_form_component(x) }
q="institution=#{fe.call(unit)}&funding=#{fe.call(osn)}&education_level=#{fe.call(edlevel)}&education_form=#{fe.call(edform)}&specialty=#{id}"


s = httpToString(@mainUrl+q, post:false, headers:hdrs, params:parameters, debug:true)

s1 = /<div class="col-md-8 col-lg-9 content">.*?<\/table>.*?<\/div>\s*<\/div>\s*<\/div>/m.match(s)
dts = /<div class="centered-h">Рейтинг на момент.*?<\/div>/m.match(s)
if s1.nil? 
  printf "Не найден список: spec=%s\n", spec
  next
end
s1=s1[0]
puts dts[0]
specName = /(\d\d\.\d\d\.\d\d)/m.match(spec)
dt = /(\d+)\.(\d+)\.(\d+),\s+(\d+)\:(\d+)/m.match(dts[0])
hr = dt[4].rjust(2,'0')
mm = dt[5].rjust(2,'0')
d="#{dt[3]}.#{dt[2]}.#{dt[1]}.#{hr}.#{mm}"
filename=getFileName(@dir,spec+'-'+prefix,d,'html')

#plan = /(План приема|Вакантные места) - (\d+)/.match(s2[0])
#printf "%s %s %s %s\n", prefix,spec,d,specName[1]
#zz= s1.scan(/<td>Да.*?<\/td>[^<]*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<\/tr>/)
#origNum = zz.size()
#zz= s1.scan(/<td>Нет<\/td>[^<]*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<\/tr>/)
#copyNum = zz.size()
#totalNum = origNum+copyNum
# <tr>
# <td>541</td>
# <td>541</td>
# <td>199-186-212 22</td>
# <td>3</td>
# <td>0</td>
# <td>0</td>
# <td>0</td>
# <td>3</td>
# <td>Нет</td>
# <td>Бюджетная основа</td>

# </tr>
File.open(filename, 'w') {|f| 
f.write '<html><head><title>'+specName[1]+'</title>'
f.write '<meta http-equiv=\'Content-Type\' content=\'text/html; charset=UTF-8\'/>'
f.write '</head><body>'
s=s1
fixE(s)
f.write s
f.write "</body></html>\n"
}
end

end



