﻿require_relative 'pkutils'
require 'nokogiri'

@dir = 'bgpu'
Dir.mkdir(@dir) unless Dir.exist?(@dir) 

@mainUrl = 'https://lk.bspu.ru/guest/competition-lists/1'
path ="#{@dir}/list.html"

if File.exist?(path)
    f = File.open(path)
    q= IO.read(f)
    q.force_encoding('UTF-8')
else    
    q = httpToFile(@mainUrl, path)
end
dt = Time.now.strftime("%Y.%m.%d.%H.%M")
doc = Nokogiri::HTML(q)

s = doc.xpath('//div[@class="space-y-6"]/div')
s.each do |v|

    specText = v.xpath('./div[1]/span[1]/text()')
    specId = specText.to_s[/\((\d\d.*?)\)/,1].gsub(' ','_')
    puts specId
    next unless specText.to_s.include?('очн')
    i=0
    
    v.xpath('./div[2]/ul/li/a').each do |a|
        i=i+1
        href = a.attr('href')
        kon = a.xpath('./div[1]/div[1]/div[1]/text()').to_s.strip
        next if kon.include?('договор')
        puts "#{specText} #{href} #{kon}"
        fname = getFileName(@dir,"#{specId}_#{i}",dt,"htm")
        httpToFile(href, fname)
    
    end
    next

    i+=1
end
