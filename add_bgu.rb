require_relative 'pkdb'
require 'nokogiri'
require 'set'


vuz="БГУ"
files = Dir.glob("bgu/bgu*.html") 

initDBs(vuz)

dbc=@db
mag=false

for f in files do

dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s=IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

num=1
numOrig=1
specID='?'
mesta=Hash.new

doc = Nokogiri::HTML(s)
sp = doc.xpath("//table/tr[4]/td/text()").to_s.strip()
puts "`````#{sp}````"
specID=sp[/- .*?(\d\d\.\d\d\.\d\d_.*?)_/,1]

if not specID 
print("Error #{f}")

end
spec=sp[/- .*?(\d\d\.\d\d\.\d\d.*?)_О_Б/,1]
mest=s[/К зачислению: (\d+)/,1]
cel=0

puts "-----#{specID}------#{spec}----"
 
  mag=false
  if specID.match?(/\d\d\.04\.\d\d/)
    dbc = @dbm
    mag = true
  else
    dbc = @db
  end

  dbc.addSpec(specID, vuz, spec, mest, cel)
  mesta[specID]=mest
if specID==='?'
  puts "Error"
  next
end
next if dbc.hasDateFlag(specID, vuz, dt)

print "Магистратура\n"  if mag 

doc.xpath("//table/tr").each do |row|
if mag
 name = row.xpath("./td[2]/a/text()").to_s
 ball = 0
 org = row.xpath("./td[8]/text()").to_s
 b1=0
 b2=0
 b3=0
else
 name = row.xpath("./td[2]/text()").to_s.strip()
 ball = row.xpath("./td[3]/text()").to_s.to_i
 orig = row.xpath("./td[9]/text()").to_s.strip!=''
 b1 = row.xpath("./td[5]/text()").to_s.to_i
 b2 = row.xpath("./td[6]/text()").to_s.to_i
 b3 = row.xpath("./td[7]/text()").to_s.to_i
end
 next unless name.length>3
 #puts row
# puts "#{name} #{ball} #{b1}+#{b2}+#{b3} #{orig} #{dt}"

 next if name.nil? or name === ''
 #name=normal(name)
 #fio=toFio(name)
 fio=name
 
 dbc.addRow(name, fio, orig, ball, vuz, specID, num, numOrig,b1,b2,b3,dt)
 numOrig+=1 if orig
 num+=1
end
puts "#{specID}: #{numOrig}/#{num}/#{mesta[specID]}"
end

commitDBs()


