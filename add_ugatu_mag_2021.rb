require_relative 'pkdb'
require 'nokogiri'
require 'set'

db = PKDB.new("data_all_mag.db")

db.createTables()
vuz="УГАТУ"
db.cleanTablesIfNeeded(vuz)

files = Dir.glob("usatu_mag/*.html") 

db.prepare()

for f in files do
dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)


spec = s[/<h4>([^<]*), УГАТУ/]

#puts spec
specID = spec[/\d\d\.\d\d\.\d\d/]
specName = spec[/\d\d\.\d\d\.\d\d (.*), УГАТУ/,1]

cel=0
puts "#{specID} #{specName}"

next if db.hasDateFlag(specID, vuz, dt) or s.length<10000

doc = Nokogiri::HTML(s)

headers = doc.xpath("//div[@class='table-info-label']")

#for h in headers do
#title = h.xpath("./h4/text()")
#info = h.xpath("./div[@class='info']/text()").text().strip()
#puts title, info
#puts "----"
#end

m_cel=headers[0].xpath("./div[@class='info']/text()").text().strip().match(/Всего мест: (\d+).*?(\d+)/)
if headers[1]
m_osn=headers[1].xpath("./div[@class='info']/text()").text().strip().match(/Всего мест: (\d+).*?(\d+)/)
else
m_osn=m_cel
end
#puts m_os[1],m_cel[1],m_osn[1]

db.addSpec(specID, vuz, specName, m_osn[1].to_i+m_cel[1].to_i,m_cel[1].to_i) #if f.include?('Общий')

tb = doc.xpath("//table/tbody")
num=1
numOrig=1
ballforplace =  Array.new(1000,1000)
flag=false
for i in [0,1] do
next unless tb[i]
tb[i].xpath("./tr").each do |row|
 puts row
 name = row.xpath("./td[3]/text()").to_s
 next if name.empty?
 ball = row.xpath("./td[4]/text()").to_s.to_i
 b1 = row.xpath("./td[5]/text()").to_s.to_i
 org = row.xpath("./td[7]/text()").to_s
 if org=~/.*Да.*/ 
   orig=true
 else
   orig=false
 end
 puts "#{name} #{ball} #{orig}"
 next if name.nil? or name === ''
 name=normal(name)
 fio=toFio(name)
 if i<100 
  if orig
    ballforplace[numOrig] = ball
    #puts "orig #{ball} ==> #{numOrig}"

  else
    if !flag
      ballforplace[numOrig..]=-1
      #puts ballforplace.to_s
      flag=true
    end
    numOrig = ballforplace.bsearch_index { |x| x<ball } 
    #puts "#{ball} ==> #{numOrig}"
  end
 end 
 db.addRow(name, fio, orig, ball, vuz, specID, num, numOrig,b1,0,0,dt)
 numOrig+=1 if orig
 num+=1
end
end
puts "#{num-1} заявлений в #{f}"
end

db.commit()




