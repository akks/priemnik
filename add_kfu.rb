require_relative 'pkdb'
require 'nokogiri'
require 'set'

vuz="КФУ"

db = PKDB.new("data_all.db")
dbm = PKDB.new("data_all_mag.db")

db.createTables()
dbm.createTables()


db.cleanTablesIfNeeded(vuz)
dbm.cleanTablesIfNeeded(vuz)

db.prepare()
dbm.prepare()


files = Dir.glob("kfu/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new


for f in files do

mag=false
if f.include?('агистратура')
  dbc = dbm
  mag = true
else
  dbc = db
end

dt = getDateFromName(f)
print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)


doc = Nokogiri::HTML(s)

#specID = f[/\d+\-(.*)\.html/,1]
spec = doc.xpath("/html/head/title/text()").text
specID = spec

num=1
numOrig=1
while specs.include?(specID)
specID+="'"
end

next if dbc.hasDateFlag(specID, vuz, dt)

dbc.addSpec(specID, vuz, spec, 1, 1)

tb = doc.xpath("//table/tbody")

ball = 0
tb.xpath("./tr").each do |row|

 num = row.xpath("./td[1]/text()").to_s.to_i
 name = row.xpath("./td[2]/text()").to_s
 name=normal(name)
 if name.nil? or name.empty?
    next
 end
 fio=toFio(name)
# print fio+', '
 origs = row.attr('style').to_s 
 if origs=~/bold/
   orig=true
 else
   orig=false
 end
#puts orig
if mag
 ball = row.xpath("./td[3]/text()").to_s.to_i
 dbc.addRow(name, fio, orig, ball, "КФУ", specID, num, numOrig,0,0,0,dt)
else
 b1 = row.xpath("./td[3]/text()").to_s.to_i
 b2 = row.xpath("./td[4]/text()").to_s.to_i
 b3 = row.xpath("./td[5]/text()").to_s.to_i
 ball = row.xpath("./td[7]/text()").to_s.to_i
# puts "#{specID} #{name}: #{ball} #{b3}"
 dbc.addRow(name, fio, orig, ball, "КФУ", specID, num, numOrig,b1,b2,b3,dt)
end
 numOrig+=1 if orig
end

puts "#{num} заявлений"
end

db.commit()
dbm.commit()



