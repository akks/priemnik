require 'fileutils'

Dir.mkdir('usatumagr') unless Dir.exist?('usatumagr')
Dir.mkdir('magistr') unless Dir.exist?('magistr')

Dir.glob('usatumagr/*.html').each { |f| File.delete(f) }
Dir.glob('tmpm/*.html').each do |f| 
 print f+"\n"
 FileUtils.cp(f, 'usatumagr')
 FileUtils.mv(f, 'magistr')
end
