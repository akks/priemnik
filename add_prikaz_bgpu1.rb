﻿require_relative 'pkutils'
require_relative 'pkdb'
require 'strscan'
require 'set'

vuz="ЗАЧИСЛЕН-БГПУ"
vuz2="ЗАЧИСЛЕН-БГПУ2"

db = PKDB.new("data_all.db")
db.createTables()
db.cleanTables(vuz)
db.cleanTables(vuz2)

db.prepare()
files = Dir.glob("bgpu_p/*.txt") 

specs=SortedSet.new

db.addSpec("?", vuz, "?", 1, 1)
db.addSpec("?", vuz2, "?", 1, 1)
specID='?'

for f in files do

s=IO.read(f)
s.force_encoding('UTF-8')
fixE(s)	   

print "File #{f}\n" 

sc = StringScanner.new(s)


loop do

break if sc.scan_until(/([А-Я][А-Яа-я\-]+\s[А-Я][А-Яа-я\-]+\s[А-Я][А-Яа-я\-]+)\s+\d/).nil?
	nm = sc[1]
	ball = 0
next unless nm
	name=normal(nm)
next if name===''
puts name
	fio=toFio(name)
if f =~ /0308/
	db.addRow(name, fio, true, ball, vuz, specID, 1,1,0,0,0)
else
	db.addRow(name, fio, true, ball, vuz2, specID, 1,1,0,0,0)
end
break if sc.eos?

end
end


db.commit()
