require 'nokogiri'
require_relative 'pkutils'

token = 'XpDZ5NVU1wjVQuiDa6QYzLB6WZD1WVfi'

mainUrl='https://ugatu.su/abitur/ratelist/bachelor/'

Dir.mkdir('usatu') unless Dir.exist?('usatu') 

puts 'Getting specs list'

if File.exist?('usatu/specs.htm')
    f = File.open('usatu/specs.htm')
	q= IO.read(f)
	q.force_encoding('UTF-8')
#	fixE(s)
else
	q = httpToFile(mainUrl,'usatu/specs.htm')
end

doc = Nokogiri::HTML(q)
allspecs={'Специалитет'=>[ ], 'Бакалавриат'=> [ ]}


s = doc.xpath('//select[@name="specValue"]/option/@value')
s.each do |v|
  next if v.text.empty?
  if v =~ /\d\d\.05\.\d\d/
    allspecs['Специалитет'].push(v.text)
  else
    allspecs['Бакалавриат'].push(v.text)
  end
  puts v
  puts v.parent.text
end
if allspecs['Специалитет'].empty?
  allspecs['Специалитет'] = ['1422','1424','1427','1433','1439','1441','1446','1447','1454','1461']
end

puts 'finished specs list'


  
allspecs.each do |level, specs|
puts level
for spec in specs do
puts spec
parameters = {
  'unit' => 'Головной ВУЗ',
  'edform' => 'Очная',
  'EducationLevel' => level,
  'specValue' => spec,
  'doc' => 'Все',
  'docOsn' => 'Бюджет',
  'comment' => 'Все',
  'csrfmiddlewaretoken' => token
}

hdrs={ 'Upgrade-Insecure-Requests' => '1',
  'Accept' => 'text/html',
  'Cache-Control' => 'max-age=0',
  'Cookie' => 'csrftoken='+token,
  'Origin' => 'https://ugatu.su',
  'Referer' => mainUrl,
}

s = httpToString(mainUrl, post:true, headers:hdrs, params:parameters, debug:false)

s1 = /<div[^>]*tableList.*?<\/table>.*?<\/div>/m.match(s)
s2 = /<h4><span>.*?<\/div>.*?<\/div>.*?<\/div>/m.match(s)

if s1.nil? or  s2.nil? 
  printf "Не найден список: spec=%s\n", spec
  next

end
#specName = /<br>\s+(\d\d\.\d\d\.\d\d.*?) </m.match(s2[0])
specName = /"selected">(\d\d\.\d\d\.\d\d)/m.match(s)
#Дата формирования: 2 июля 2018 г. 18:13:05
dt = /(\d+)\.(\d+)\.(\d+)\s+(\d+)\:/m.match(s2[0])

#day=dt[1].rjust(2,'0')
#mn=dt[2]
#mn.sub!('июня','07')
#mn.sub!('июля','08')
#mn.sub!('августа','09')
puts "SpecName not found: #{s2[0]}" if specName.nil?
puts "Date not found: #{s2[0]}" if dt.nil?
hr = dt[4].rjust(2,'0')

d="#{dt[3]}.#{dt[2]}.#{dt[1]}.#{hr}"
filename=getFileName('usatu',specName[1]+'-'+level,d,'html')

plan = /(План приема|Вакантные места) - (\d+)/.match(s2[0])
printf "%s %s %s %s\n", level,spec,d,specName[1]


zz= s1[0].scan(/<td>Да<\/td>[^<]*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<\/tr>/)
origNum = zz.size()
zz= s1[0].scan(/<td>Нет<\/td>[^<]*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<td>[^>]*<\/td>\s*<\/tr>/)
copyNum = zz.size()
totalNum = origNum+copyNum


File.open(filename, 'w') {|f| 
f.write '<html><head><title>'+origNum.to_s+'/'+plan[2]+' '+specName[1]+'</title>'
f.write '<meta http-equiv=\'Content-Type\' content=\'text/html; charset=UTF-8\'/>'
f.write '</head><body>'
f.write '<h4> Всего '+totalNum.to_s+', оригиналов '+ origNum.to_s+', копий '+copyNum.to_s+'</h4>'
f.write s2
f.write "\n"
s=s1[0]
fixE(s)
f.write s
f.write "</body></html>\n"

}


end

end



