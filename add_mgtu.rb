require_relative 'pkdb'
require 'set'

db = PKDB.new("data_all.db")

vuz = "МГТУ"
db.createTables()
db.cleanTablesIfNeeded(vuz)

files = Dir.glob("mgtu/*.txt") 

specs=SortedSet.new


db.prepare()

for f in files do

dt = getDateFromName(f)
print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)	   

m=/курс на (\d\d)\.(\d\d).(\d\d\d\d)/.match(s)

dt ="#{m[3]}.#{m[2]}.#{m[1]}"
s.scan(/\s+(.*?)\s+[А-Я]\d\d\d\d\s+(.*)/) do |name,sp|
	name=normal(name)
	fio=toFio(name)

noNeed=false
sp.scan(/\d\d\.\d\d\.\d\d/) do |spec|
  #puts spec 
 #puts fio
	unless specs.include?(spec)
	   	specs.add(spec)
	   	db.addSpec(spec, "МГТУ", spec+':', 1,1)
	end
	if db.hasDateFlag(spec, vuz, dt)
	    noNeed=true
	    break
	end
	db.addRow(name, fio, false, 0, "МГТУ", spec, 1,1,0,0,0,dt)
end
break if noNeed
end

end

db.commit()




