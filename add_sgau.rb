require_relative 'pkdb'
require 'nokogiri'
require 'set'

db = PKDB.new("data_all.db")

db.createTables()
vuz="СГАУ"
db.cleanTablesIfNeeded(vuz)

files = Dir.glob("sgau/*.html") 

specs=SortedSet.new

db.prepare()

for f in files do
dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

next if s.include?('по направлениям магистратуры')

spec = s[/<h5.*?> ([^<]*)</]

puts spec
specID = spec[/\d\d\.\d\d\.\d\d/]
specName = spec[/\d\d\.\d\d\.\d\d (.*)</,1]


plan = s[/план при[еЁ]ма - (\d+)/,1]

cel=0
puts "#{specID} (#{plan} мест) #{specName}"

db.addSpec(specID, "СГАУ", specName, plan.to_i, cel.to_i) #if f.include?('Общий')
next if db.hasDateFlag(specID, vuz, dt)

doc = Nokogiri::HTML(s)
tb = doc.xpath("//table/tbody")

num=1
numOrig=1

tb.xpath("./tr").each do |row|
# puts row
 name = row.xpath("./td[3]/text()").to_s
 next if name.empty?
 ball = row.xpath("./td[4]/text()").to_s.to_i
 b1 = row.xpath("./td[5]/text()").to_s.to_i
 b2 = row.xpath("./td[6]/text()").to_s.to_i
 b3 = row.xpath("./td[7]/text()").to_s.to_i
 org = row.xpath("./td[9]/text()").to_s
 if org=~/.*Да.*/ 
   orig=true
 else
   orig=false
 end
# puts "#{name} #{ball} #{orig}"
 next if name.nil? or name === ''
 name=normal(name)
 fio=toFio(name)
 db.addRow(name, fio, orig, ball, "СГАУ", specID, num, numOrig,b1,b2,b3,dt)
 numOrig+=1 if orig
 num+=1
end
puts "#{num} заявлений"
end

db.commit()




