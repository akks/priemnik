﻿require_relative 'pkutils'
require 'nokogiri'
@dir = 'bgu'

dt = Time.now.strftime("%Y.%m.%d.%H")
filials={1=>'-Уфа',5=>'-Бирск',3=>'-Сибай',2=>'-Стерлитамак', 4=>'-Нефтекамск'}

#for fl in [1,2,3,4,5] do
url="https://pk-inf.bashedu.ru/rating"
path="#{@dir}/lists.html"
puts path
q=''
if File.exist?(path)
  File.open(path, 'rb') do |f| 
    q = f.read
  end
else
  q = httpToFile(url, path, post:false)
end
specs=Set.new

i=0
addrs = Hash.new
Nokogiri::HTML(q).xpath("//div[@class='d-td1']/button").each { |opt|
  htmlname=opt.attr('data-filename')

  spec=opt.text().strip
  next unless spec =~ /(бакалавр|магистр), очная, бюджетная основа$/ and htmlname.include?('БашГУ')
  spec.gsub!(/, (бакалавр|магистр), очная, бюджетная основа$/,'')
  m=spec.match(/(\d\d\.\d\d\.\d\d)\s(.*)/)
  specID=htmlname[/\d\d\.\d\d\.\d\d_[А-Я]+/]
  #specID = m[1]
  specName = m[2]
  puts "#{specID}:  #{specName}"
  addr = 'https://pk-inf.bashedu.ru/static/file_vault/'+URI.encode_www_form_component(htmlname).gsub('+','%20')
  s=httpToString(addr, post:false)
  filename=getFileName(@dir,specID,dt,'html')
  puts "ответ получен от #{addr} сохраняем в #{filename}"
  File.open(filename, 'wb') {|f|
    f.write s
  }
  sleep(5)
}


