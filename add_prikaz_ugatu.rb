﻿require_relative 'pkutils'
require_relative 'pkdb'
require 'strscan'
require 'set'

vuz="ЗАЧИСЛЕН-УГАТУ"

db = PKDB.new("data_all.db")
db.createTables()
db.cleanTables(vuz)

db.prepare()
#"https://www.prkomm.ugatu.su/media/uploads/2016/Bachelor/Prikazy/2201.pdf"

urls=[
'https://www.ugatu.su/media/uploads/2018/Bachelor/Orders/1990.pdf',
'https://www.ugatu.su/media/uploads/2018/Bachelor/Orders/1992.pdf',
'https://www.ugatu.su/media/uploads/2018/Bachelor/Orders/2001.pdf',
'https://www.ugatu.su/media/uploads/2018/Bachelor/Orders/2002.pdf'
]


i=0
urls.each do |url|
i+=1
fname = getFileName("ugatu_p",i.to_s,'',"pdf")
httpToFile(url, fname)
convertPDFToText(fname, fname+'.txt')

s=IO.read(fname+'.txt')
s.force_encoding('UTF-8')
fixE(s)	   

dt ="03.08.2018"
print "File #{fname} : #{dt}\n" 

sc = StringScanner.new(s)

numOrig=1
num=1

spec=''
specID=''
skip=false

loop do

break if sc.scan_until(/\n(\s*(\d\d\.\d\d\.\d\d) (.+?)\()|(\s*([А-Я][А-Яа-я ]+)[ ]+([\d\-]+))|(\nФилиал в г\. ([А-Яа-я\-]+))/m).nil?

if sc[1]
#puts "~~~#{sc[1]}~~~"
	specID = sc[2]
	spec = sc[3]
	db.addSpec(specID, vuz, spec, 1, 1)
	numOrig=1
	num=1
	puts "Spec: #{specID}: #{spec}"
	skip=false
#	exit
else if sc[7]
   	puts sc[7]
	skip=true
else

next if skip
#puts "---#{sc[4]}---"
	nm = sc[5]
	ball = sc[6].to_i
next unless nm
	name=normal(nm)
next if name===''
puts name+' ===>>> '+ball.to_s
	fio=toFio(name)
	db.addRow(name, fio, true, ball, vuz, specID, num,numOrig,0,0,0,dt)
	num+=1
end
end
break if sc.eos?

end

end


db.commit()
