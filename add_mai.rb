require_relative 'pkdb'
require 'strscan'
require 'set'


db = PKDB.new("data_all.db")
db.createTables()

vuz="МАИ"
db.cleanTablesIfNeeded(vuz)


files = Dir.glob("mai/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new


db.prepare()
tm0=Time.now

for f in files do

dt = getDateFromName(f)
print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

sc = StringScanner.new(s)


loop do

break if sc.scan_until(/(\d\d\.\d\d\.\d\d) "(.*?)"/).nil?

specID = sc[1]
spec = sc[2]
print "     #{specID}: #{spec}"

next if db.hasDateFlag(specID, vuz, dt)
db.addSpec(specID, vuz, spec, 1, 1)
n=0
while sc.scan_until(/<tr><td>(.*?)<\/td><td>(.*?)<\/td>/m)
	name=normal(sc[2])
	fio=toFio(name)
	db.addRow(name, fio, false, 0, vuz, specID, 1,1,0,0,0,dt)
	#puts name
	n=n+1
end
puts "----  #{n} заявлений"

break if sc.eos?

end
end

db.commit()
puts Time.now-tm0
