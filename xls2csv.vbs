Dim oExcel
Set oExcel = CreateObject("Excel.Application")
Dim oBook

Set objFSO = CreateObject("Scripting.FileSystemObject")

objStartFolder = "d:\priemnik\hse"
Set objFolder = objFSO.GetFolder(objStartFolder)
Set colFiles = objFolder.Files

For Each f in colFiles
    if right(f,3) = "xls" then
	 Set oBook = oExcel.Workbooks.Open(f)
	 oBook.SaveAs f & ".csv", 6
	 oBook.Close False
    end if
Next
oExcel.Quit
