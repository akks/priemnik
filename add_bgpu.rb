require_relative 'pkdb'
require 'set'
require 'nokogiri'

vuz='БГПУ'

db = PKDB.new("data_all.db")

db.createTables()
db.cleanTables(vuz)

dbm = PKDB.new("data_all_mag.db")
dbm.createTables()
dbm.cleanTables(vuz)


files = Dir.glob("bgpu/*.htm") 
specs=SortedSet.new

db.prepare()
dbm.prepare()

for f in files do
	print "File #{f} \n" 
	dt = getDateFromName(f).gsub('-','.')
	specID = f[/\d-(\d\d.*?.)\.htm/,1]

	s= IO.read(f)
	s.force_encoding('UTF-8')
	fixE(s)
	next unless s.match(/Очная форма обучения/)

	doc = Nokogiri::HTML(s)

	specName = doc.xpath('//div[@class="space-y-2"]/div[1]/div[1]/span/text()').to_s
	
	#if s.match(/Общий конкурс/)
	m=s[/Количество мест: (\d+)/,1]
	puts "#{specID} / #{specName} / #{m}"
	db.addSpec(specID, vuz, specName, m.to_i,0)
	
	
	
	tb = doc.xpath("//table/tbody/tr")
	num=1
	numOrig=1

	tb.each do |row|
	# puts row
	 name = row.xpath("./td[2]/text()").to_s.strip
	 next if name.empty?
	 ball = row.xpath("./td[3]/text()").to_s.to_i
	 org = row.xpath("./td[7]/text()").to_s
	 if org=~/.*Да.*/ 
	   orig=true
	 else
	   orig=false
	 end
	 #puts "#{name} #{ball} #{orig}"
	 #name=normal(name)
	 #fio=toFio(name)
	 fio=name
	 db.addRow(name, fio, orig, ball, vuz, specID, num, numOrig,0,0,0,dt)
	 numOrig+=1 if orig
	 num+=1
	end
	puts "#{num-1} заявлений в #{f}"
end
	
db.commit()
dbm.commit()
	
	
	