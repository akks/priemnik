require_relative 'pkdb'
require 'set'

def fixDate(tm)
	tm.gsub(/2019\.(\d+)\.(\d+)(\.\d+)?/,'\2.\1')
end

tm0=Time.now

@dbname='data_all.db' if @dbname.nil?
@mainvuz='УГАТУ' if @mainvuz.nil?
@magistr=false if @magistr.nil?
@hourly=false if @hourly.nil?

if @magistr
   if @hourly
	@fName='changesMH.html' 
   else
	@fName='changesM.html' 
   end
else
   if @hourly
	@fName='changesH.html' 
   else
	@fName='changes.html' 
   end
end    

puts @fName

db = PKDB.new(@dbname)
db.prepare(false)

if(@magistr)
baseSpec='01.04.02'
else
baseSpec='01.03.02'
end

rs = db.execQuery("select dt from dateFlags where vuz='УГАТУ' and specID='#{baseSpec}' order by dt desc limit 50")
rs.each do |dt| 
   print(dt)
end

return if rs[0].nil? or rs[1].nil? 
if @hourly
date1 = rs[0][0].to_s
date2 = rs[1][0].to_s
else
date1 = rs[0][0].to_s
dayPart=date1[0..9]
puts dayPart
rs.each do |dt| 
   print(dt)
  if dt[0][0..9] != dayPart
     date2=dt[0].to_s
     break
  end
end
end


puts "Changes from #{date2} to #{date1}"


specs = Hash.new
changes = Hash.new
changesB = Hash.new
changesR = Hash.new
changesO = Hash.new
changesOR = Hash.new
newBalls = Hash.new
nums = Hash.new
origs = Hash.new
nums2 = Hash.new
origs2 = Hash.new
places = Hash.new

db.execQuery("select spec,specName,mest from specs where vuz='УГАТУ'").each do |id, spec,mest|
   specs[id]=spec
   places[id]=mest
   newBalls[id]=Hash.new
   changes[id]=[]
   changesB[id]=[]
   changesR[id]=[]
   changesO[id]=[]
   changesOR[id]=[]
end

sql="SELECT NAME,SPEC,BALL FROM RECORDS WHERE vuz='УГАТУ' and (tm='#{date1}' or tm='#{date2}') order by spec,name,tm"

db.execQuery(sql).each do |name,spec,ball|
   newBalls[spec][name]=ball
end

sql="SELECT NAME,SPEC,BALL FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date1}' EXCEPT SELECT NAME,SPEC,BALL FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date2}' order by spec,name"

db.execQuery(sql).each do |name,spec,ball|
   changes[spec].push(name)
end

sql="SELECT NAME,SPEC,BALL FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date2}' EXCEPT SELECT NAME,SPEC,BALL FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date1}' order by spec,name"
db.execQuery(sql).each do |name,spec,ball|
   if changes[spec].include?(name) 
      changesB[spec].push("Б #{name}: #{ball}->#{newBalls[spec][name]}")
      changes[spec].delete(name)
   else
      changesR[spec].push(name)
   end
end

sql="SELECT NAME,SPEC FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date1}' and orig='true' EXCEPT SELECT NAME,SPEC FROM RECORDS WHERE vuz='УГАТУ' and orig='true' and tm='#{date2}' order by spec,name"

db.execQuery(sql).each do |name,spec|
   changesO[spec].push(name)
end

sql="SELECT NAME,SPEC FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date2}' and orig='true' EXCEPT SELECT NAME,SPEC FROM RECORDS WHERE vuz='УГАТУ' and orig='true' and tm='#{date1}' order by spec,name"

db.execQuery(sql).each do |name,spec|
   changesOR[spec].push(name)
end


sql="SELECT spec, COUNT(1) as zs, sum(orig=='true') as origs FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date1}' group by spec"
db.execQuery(sql).each do |spec,num,numOrig|
   nums [spec]=num
   origs[spec]=numOrig
end

sql="SELECT spec, COUNT(1) as zs, sum(orig=='true') as origs FROM RECORDS WHERE vuz='УГАТУ' and tm='#{date2}' group by spec"
db.execQuery(sql).each do |spec,num,numOrig|
   nums2 [spec]=num
   origs2[spec]=numOrig
end

   f = File.open(@fName, 'w') 
   f.write '<html><head><title>Список специальностей</title>'
   f.puts ' <style type="text/css">
   table { 
   width:100%;
   font-size:100%
   }
   table tr {
     background-color: lightgreen;
     vertical-align: top;
     border-style: solid;
   }
   table tr td {
   }
  
   .spec {
        background-color: white;
    } 
   .orig {
     background-color: khaki;
    } 
   </style>'
    f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
    f.write "</head><body>"

    f.write "<h2>Изменения между #{date2} и #{date1}</h2>"

    f.write '<table><tbody>'

specs.keys.sort.each do |id|
   next if nums[id].nil? or nums2[id].nil?
   puts nums[id].to_s+" - "+nums2[id].to_s

   f.puts "<tr class='spec'><td width='400px'>#{specs[id]}</td><td>#{nums2[id]}+#{nums[id]-nums2[id]} = <b>#{nums[id]}</b> &nbsp;&nbsp;&nbsp;&nbsp; #{origs2[id]}+#{origs[id]-origs2[id]}=<b>#{origs[id]}</b></td>"

   changes[id].each do |txt|
   f.puts "<tr><td></td><td>+ #{txt} (#{newBalls[id][txt]})</td>"
   end
   changesR[id].each do |txt|
   f.puts "<tr><td></td><td>− #{txt} (#{newBalls[id][txt]})</td>"
   end
   changesO[id].each do |txt|
   f.puts "<tr><td></td><td>+O #{txt} (#{newBalls[id][txt]})</td>"
   end
   changesOR[id].each do |txt|
   f.puts "<tr><td></td><td>−O #{txt} (#{newBalls[id][txt]})</td>"
   end
   changesB[id].each do |txt|
   f.puts "<tr><td></td><td>#{txt}</td>"
   end
end


    f.puts '</tbody></table></body></html>'
