require_relative 'pkdb'
require 'set'
require 'nokogiri'
tm0=Time.now

vuz='УГНТУ'
initDBs(vuz)

mfiles=['ugntu/bakalavr.htm', 'ugntu/specialist.htm', 'ugntu/magistr.htm']

idRegex = /\(([А-Я]{2,4}\d*[а-я]?(,\s*[А-Я]{2,4}\d*[а-я]?)?)\)/

mfiles.each do |f|

q = File.open(f)
doc = Nokogiri::HTML(q)

s = doc.xpath("//table")
# puts s

specID = ''
flag=true
s.xpath('//tr').each do |row|
 id = row.xpath('./td[1]/a/text()').to_s
# ids = row.xpath("./td[1]/div/strong/text()").to_s
# flag=false if ids =~ /\s*Филиалы\s*/
# next if flag
# break if ids =~ /.*Салават.*/
# t = c[0].element_children[0].text
next if id.nil? 
specID = id[idRegex,1]
specName = id[/\d.*\)/]
next if specID.nil? 
mest = row.xpath("./td[2]/text()").to_s
cmest = row.xpath("./td[4]/div/text()").to_s
mest='0' unless mest=~/\d+/
cmest='0' unless cmest=~/\d+/
puts "#{specID}: #{specName} 		#{mest} #{cmest}"
if f =~ /.*mag.*/
  @dbm.addSpec(specID, "УГНТУ", specName, mest.to_i,cmest.to_i)
else
  @db.addSpec(specID, "УГНТУ", specName, mest.to_i,cmest.to_i)
end
end
end

files = Dir.glob("ugntu/*.html") 

specs = SortedSet.new

plans = {}
cels = {}

for f in files do

s = IO.read(f)
s.force_encoding('UTF-8')
fixE(s)
mag = f.match(/-\d\d\.04\.\d\d/)

s1 = /<table border.*?<\/table>/m.match(s)
next if s1.nil? 
specID = f[/([А-Я]{2,4}\d*[а-я]?(,\s*[А-Я]{2,4}\d*[а-я]?)?)/m, 1]

# plan = /План приема - (\d+)/.match(s)
# cel = /целевой прием - (\d+)\,/.match(s2[0])
dt = getDateFromName(f)
#dt = /<h4>\s*(.*?)\s*: всего/.match(s)[1]

# or dt.nil? or plan.nil? or cel.nil?

if mag
  dbc=@dbm
else
  dbc=@db
end

next if dbc.hasDateFlag(specID, vuz, dt)
print "File #{f} mag=#{mag} #{specID}: #{dt}\n" 

#specName = specName[1]
#specName[/\s+\(г\. Уфа\)/]='' 
#specName+='-П/Б' if f =~ /Прикладной/
#specName+='-спец.' if f =~ /Специалитет/
origNum = s1[0].scan(/>Оригинал</).size()
copyNum = s1[0].scan(/>Копия</).size() 
totalNum = origNum + copyNum

#celPlan = cel[1].to_i
#totalPlan = plan[1].to_i
#totalPlan = 1 if totalPlan ==0

#spec= specName

#specs.add(spec)

#plans[spec]=totalPlan
#cels[spec]=celPlan


#specID= spec[/\b([А-Я]{2,4}[а-я]?)\b/,1]
puts specID
#/\d\d\.\d\d\.\d\d/.match(spec)[0]
#specID+= 'п' if spec =~ /.*П\/Б.*/

#dbc.addSpec(specID, "УГНТУ", spec, 1, 1)
#db.exec("UPDATE SPECS SET specName='#{spec}' WHERE vuz='УГНТУ' and spec='#{specID}'")
#<tr ><td align="center">2. </td>
#<td align="left" >Сакаев Р. И</td>
#<td >бюджет</td>
#<td align="center" ></td>
#<td >копия</td>
#<td >ИЕ-0;МЕ-0;РЯЕ-0;</td>
#<td style="width:75px;"></td>
#</tr>

# <tr >
# <td align="center">1. </td>
# <td align="left" >Абделазиз А. Р</td>
# <td style="width:85px;">Платное</td>
# <td >Ид;М;РЯ;Ф;</td>
# <td >Заявление принято</td>
# </tr>



numOr=1

#s1[0].scan(/<tr.*?<td.*?\/td>[^<]*?<td.*?>(.*?)</m) do 
s1[0].scan(/<tr.*?<td.*?>(.*?)\.\s*<\/td>.*?<td.*?>.*?<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>.*?<\/td>.*?<td.*?>.*?<\/td>.*?<td.*?>.*?<\/td>.*?<td.*?>(.*?)<\/td>/m) do 
   |row| 
#puts "---"
   num = row[0]
   name = row[1]
   bud = row[2] 
   balls = row[3]
   exams = row[5]
   orig = row[4]
   if name.include?('S-') 
     name = name.gsub(/S-(\d\d\d)(\d\d\d)(\d\d\d)(\d\d)/,'\1-\2-\3 \4')
#     puts name
   end


  # num = 1
  # name = row[0]+"."
  # bud = true 
  # balls = 0
  # exams = ''
  # orig = 'копия'

  #        МЕ-50;РЯЕ-78;ФЕ-45;
	i=0
	bs=[0,0,0,0]
  exams.scan(/\d+/) { |b|
  #	ball+=b.to_i
    if (b.to_i > 20)
      bs[i]=b.to_i
      i+=1
    end
  }
	name=normal(name)
  fio=toFio(name)
#	numOr=num
	if row[6].include?('Да')
#	if orig.include?('риг')
 	of=true
	else
	of=false
  end
  
 
  #if fio==="Шамсутдинов И. Р."
    #puts "#{name} #{bs[0]} #{bs[1]} #{bs[2]} #{of}"
  #end
    dbc.addRow(name, fio, of, balls, "УГНТУ", specID, num, numOr ,bs[0],bs[1],bs[2],dt)
    numOr=numOr+1 if of

end

end

commitDBs()

puts Time.now-tm0
