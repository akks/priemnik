SELECT records.spec,
       specName,
       tm,
       sum(orig = 'true' and ball>=230) AS origs230,
       sum(ball>=230) AS total230,
       printf("%5.2f", 110 * sum(orig = 'true' and ball>=230) / sum(ball>=230) / 1.1) AS pc230,
       sum(orig = 'true' and ball>=220 and ball<230) AS origs220,
       sum(ball>=220 and ball<230) AS total220,
       printf("%5.2f", 110 * sum(orig = 'true' and ball>=220 and ball<230) / sum(ball>=220 and ball<230) / 1.1) AS pc220,
       sum(orig = 'true' and ball>210 and ball<220) AS origs210,
       sum(ball>=210 and ball<220) AS total210,
       printf("%5.2f", 110 * sum(orig = 'true' and ball>=210 and ball<220) / sum(ball>=210 and ball<220) / 1.1) AS pc210,
       sum(orig = 'true' and ball>200 and ball<210) AS origs200,
       sum(ball>=200 and ball<210) AS total200,
       printf("%5.2f", 110 * sum(orig = 'true' and ball>=200 and ball<210) / sum(ball>=200 and ball<210) / 1.1) AS pc200,
       sum(orig = 'true' and ball>=195 and ball<200) AS origs195,
       sum(ball>=195 and ball<200) AS total195,
       printf("%5.2f", 110 * sum(orig = 'true' and ball>=195 and ball<200) / sum(ball>=195 and ball<200) / 1.1) AS pc195,
       sum(orig = 'true' and ball>=190 and ball<195) AS origs190,
       sum(ball>=190 and ball<195) AS total190,
       printf("%5.2f", 110 * sum(orig = 'true' and ball>=190 and ball<195) / sum(ball>=190 and ball<195) / 1.1) AS pc190
  FROM records
       LEFT JOIN
       specs ON records.spec = specs.spec AND 
                records.vuz = specs.vuz
 WHERE tm = (
                SELECT max(dt) 
                  FROM dateFlags
                 WHERE vuz = 'УГАТУ'
            )
AND 
       numOr <= mest and 
       records.vuz = 'УГАТУ'
 GROUP BY records.spec
 ORDER BY specName
