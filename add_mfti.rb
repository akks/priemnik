require_relative 'pkdb'
require 'set'
require 'nokogiri'

db = PKDB.new("data_all.db")
db.createTables()

vuz="МФТИ"

db.cleanTablesIfNeeded(vuz)

files = Dir.glob("mfti/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new


db.prepare()
for f in files do

dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)
	
s1 = /<table.*?<\/table>/m.match(s)
next if s1.nil?

spec=/\d\d\.(.*)\.html/.match(f)[1]

specs.add(spec)

puts spec

specID=spec
#specID+= 'п' if spec =~ /.*П\/Б.*/

db.addSpec(specID, vuz, spec, 1, 1)
next if db.hasDateFlag(specID, vuz, dt)


num=1
numOrig=1


doc = Nokogiri::HTML(s)

tb = doc.xpath("//table/tbody")

tb.xpath("./tr").each do |row|
 name = row.xpath("./td[3]/span/text()").to_s
 ball = row.xpath("./td[8]/text()").to_s
 #orig = row.xpath("./td[9]/span/text()").to_s
 orig = ! row.xpath("./td[10]").to_s.include?('checkbox_round_green_empty')

 next if name.nil? or name === ''
# puts name
 name=normal(name)
 fio=toFio(name)
 numOrig+=1 if orig
 num+=1
 db.addRow(name, fio, orig, ball.to_i, "МФТИ", specID, num, numOrig,0,0,0,dt)
end
end

db.commit()
