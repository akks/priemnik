require_relative 'pkdb'
require 'set'

db = PKDB.new("data.db")


sql = "
  delete from specs where vuz='МФТИ'; 
  INSERT INTO specs (vuz, spec, specName, mest, cel) VALUES ('МФТИ', 'Информатика и вычислительная техника', 'Информатика и вычислительная техника', 10, 1);
  INSERT INTO specs (vuz, spec, specName, mest, cel) VALUES ('МФТИ', 'Компьютерная безопасность', 'Компьютерная безопасность', 10, 1);
  INSERT INTO specs (vuz, spec, specName, mest, cel) VALUES ('МФТИ', 'Прикладная математика и информатика', 'Прикладная математика и информатика', 120, 1);
  INSERT INTO specs (vuz, spec, specName, mest, cel) VALUES ('МФТИ', 'Прикладные математика и физика ', 'Прикладные математика и физика ', 710, 1);
  INSERT INTO specs (vuz, spec, specName, mest, cel) VALUES ('МФТИ', 'Системный анализ и управление ', 'Системный анализ и управление ', 10, 1);"


db.exec(sql)
