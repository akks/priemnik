require 'net/http'
require 'uri'
require 'net/https'

token = "fSczdYS57xqiUdTU2nXO9xHosBy7HKm4"

uri = URI.parse("https://prkomm.ugatu.su/ratelist/bachelor/")

http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Post.new(uri.request_uri)

request.add_field("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4")
request.add_field( "Upgrade-Insecure-Requests","1")
request.add_field("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2756.0 Safari/537.36 OPR/40.0.2267.0 (Edition developer)")
request.add_field( "Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
request.add_field( "Cache-Control","max-age=0")
request.add_field( "Cookie","csrftoken="+token )
request.add_field( "Connection","keep-alive")
request.add_field("Host","www.prkomm.ugatu.su")
request.add_field("Origin","https://www.prkomm.ugatu.su")
request.add_field("Referer","https://www.prkomm.ugatu.su/ratelist/bachelor/")

allspecs={"Бакалавриат"=>
['325','326','327','334','335','336','337','338','340','342','343','330','344',
'345','346','347','348','349','350','351','352','353','332','355','356','357',
'333','328','359','360','361','362','364','365','366','390','391','392'],
"Специалитет"=>['339','341','388','331','389','354','358','329','363','393']}


allspecs.each do |level, specs|

for spec in specs do

parameters =  {
"unit"=>"Головной ВУЗ",
"edform"=>"Очная",
"EducationLevel"=>level,
"specValue"=>spec,
"doc"=>"Все",
"docOsn"=>"Контракт",
"comment"=>"Все",
"csrfmiddlewaretoken"=>token
}


request.set_form_data(parameters) 

response = http.request(request)


s= response.body()
s.force_encoding('UTF-8')

s1 = /<div[^>]*table[^>]*>.*?<\/div>/m.match(s)
s2 = /<h4><span>.*?<\/h4>/m.match(s)
next if s1.nil? or  s2.nil?

specName = /<br>\s+(\d\d\.\d\d\.\d\d.*?) <br>/m.match(s2[0])

dt = /по состоянию на ([^\s]+)\s(\d+)/.match(s2[0])

printf "%s %s %s %s\n", level,spec,dt[1],specName[1]

zz= s1[0].scan(/<td>Да<\/td>[^<]*<td>[^>]*<\/td>[^<]*<!--<td><\/td>-->[^<]*<td>[^>]*<\/td>[^<]*<\/tr>/)
origNum = zz.size()
zz= s1[0].scan(/<td>Нет<\/td>[^<]*<td>[^>]*<\/td>[^<]*<!--<td><\/td>-->[^<]*<td>[^>]*<\/td>[^<]*<\/tr>/)
copyNum = zz.size()
totalNum = origNum+copyNum


File.open(dt[1]+'-'+dt[2]+'-'+specName[1]+'к-'+level+'.html', 'w') {|f| 
f.write "<html><head><title>"+origNum.to_s+' '+specName[1]+"к</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write "<h4> Всего "+totalNum.to_s+", оригиналов "+ origNum.to_s+", копий "+copyNum.to_s+"</h4>"
f.write s2
f.write "\n"
f.write s1[0].gsub(/ё/,'е').gsub(/Ё/,'Е')
f.write "</body></html>\n"

}

end

end
