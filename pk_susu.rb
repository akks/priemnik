﻿require_relative 'pkdb'
require 'watir'
require 'headless'

Dir.mkdir('susu') unless Dir.exist?('susu')

httpToFile('https://abit.susu.ru/rating/2020/bachelor-intra.php','susu/spiski.htm')

require 'nokogiri'

dt = Time.now.strftime("%Y.%m.%d.%H")

q = File.open("susu/spiski.htm")
doc = Nokogiri::HTML(q)

s = doc.xpath("//table/tbody")

addrs = Hash.new

s.xpath("./tr").each do |row|
	spec = row.xpath("./td[2]/text()").text.gsub(/"/,'')
	spec.gsub!(/\s*^/,'')
	spec=spec.strip
#	puts "spec=```#{spec}```"
	lnk = row.xpath("./td[7]/a/@href").text

#next unless form === "очная" and base.include?("общее")
	puts "#{spec}--#{lnk}"
#db.addSpec("#{specID}", "УГНТУ", "#{specID}", mest.to_i,cmest.to_i)
	addrs[spec]='https://abit.susu.ru/rating/2020/'+lnk unless lnk===''
end



Headless.ly do
print 'Initializing Watir+Firefox...'
#profile = Selenium::WebDriver::Firefox::Profile.new('/home/user/.mozilla/firefox/1o16j5in.default')
b = Watir::Browser.new :firefox
#, :profile => profile
puts 'OK'

n=addrs.count
k=0
addrs.each do |spec,addr|
  fname = getFileName("susu",spec,dt)
  print "Getting page #{addr}..." 
  b.goto URI.encode(addr)
  File.open(fname, 'wb') {|f|
    f.write b.html
  }
  k=k+1
  puts "OK, #{100*k/n}%"
  sleep 0.2
end

end