require_relative 'pkdb'
require 'strscan'
require 'set'


vuz="МГУ"
initDBs(vuz)


files = Dir.glob("mgu/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new



for f in files do

dbc = @db
mag = false

if f.include?('-mag')
   mag = true
   dbc = @dbm
end

dt = getDateFromName(f)
print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

sc = StringScanner.new(s)

loop do

break if sc.scan_until(/<h4[^>]*?>(Образовательная программа|Направление|Специальность) .*?“(.*?)”/).nil?

spec = sc[2]

next if dbc.hasDateFlag(spec, vuz, dt)
dbc.addSpec(spec, vuz, spec, 1, 1)

puts sc[2]

while sc.scan_until(/<tr>.*?<td.*?<td>(.*?)<\/td>/m)
	name=normal(sc[1])
	fio=toFio(name)
	dbc.addRow(name, fio, false, 300, vuz, spec, 1,1,0,0,0,dt)
end

break if sc.eos?

end



#s.scan(/\s+(.*?)\s+[А-Я]\d\d\d\d\s+(.*)/) do |name,sp|
#	name=normal(name)#
#	fio=toFio(name)

  #puts fio
#  db.addRow(name, fio, false, 200, "МГТУ", sp, 1,1)
#end


end

commitDBs()
