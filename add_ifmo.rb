require_relative 'pkdb'
require 'nokogiri'
require 'set'

vuz="ИТМО"
initDBs(vuz)

files = Dir.glob("ifmo/*.html") 

specs=SortedSet.new

for f in files do

dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

num=1
numOrig=1

spm=/(\d\d\.\d\d\.\d\d)\s+[«-](.*?)[»<]/.match(s)
specID=spm[1]
spec=spm[2]
puts "#{specID} #{spec}"

doc = Nokogiri::HTML(s)

if doc.xpath("/html/head/title").to_s.include?('агистратур')
  puts 'Магистратура'
  dbc = @dbm
  mag = true
else
  dbc = @db
end


next if dbc.hasDateFlag(specID, vuz, dt)
dbc.addSpec(specID,vuz,spec,1,1)



doc.xpath("//table").each do |elem|
  elem.xpath("./tr").each do |row|
   if row.xpath("./td[1]/@rowspan").to_s === ''
   name = row.xpath("./td[3]/text()").to_s
   next if name === 'Номер заявления'
   next if name === 'Русский язык'
   next if name =~ /^[^\s]+$/
#   e = row.xpath("./td[4]/text()").to_s
#   next if e===''
   ball = row.xpath("./td[9]/text()").to_s.to_i
   org = row.xpath("./td[10]/text()").to_s
   b1 = row.xpath("./td[5]/text()").to_s.to_i
   b3 = row.xpath("./td[6]/text()").to_s.to_i
   b2 = row.xpath("./td[7]/text()").to_s.to_i
   else
   name = row.xpath("./td[4]/text()").to_s
   next if name === 'Номер заявления'
   next if name === 'Русский язык'
  next if name =~ /^[^\s]+$/
#   e = row.xpath("./td[4]/text()").to_s
#   next if e===''
   ball = row.xpath("./td[10]/text()").to_s.to_i
   org = row.xpath("./td[11]/text()").to_s
   b1 = row.xpath("./td[6]/text()").to_s.to_i
   b3 = row.xpath("./td[7]/text()").to_s.to_i
   b2 = row.xpath("./td[8]/text()").to_s.to_i
   end
   
   if org=~/.*Да.*/ 
     orig=true
   else
     orig=false
   end
#   puts "#{name} #{ball} #{orig}"
   next if name.nil? or name === '' or name.length<4

   name=normal(name)
   fio=toFio(name)

   dbc.addRow(name, fio, orig, ball, vuz, specID, num, numOrig,b1,b2,b3,dt)
   numOrig+=1 if orig
   num+=1
  end
 end
end

commitDBs()




