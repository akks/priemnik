require 'fileutils'

Dir.mkdir('usatu') unless Dir.exist?('usatu')
Dir.mkdir('bakalavr') unless Dir.exist?('bakalavr')

Dir.glob('usatu/*.html').each { |f| File.delete(f) }
Dir.glob('tmp/*.html').each do |f| 
 print f+"\n"
 FileUtils.cp(f, 'usatu')
 FileUtils.mv(f, 'bakalavr')
end
