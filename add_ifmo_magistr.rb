require_relative 'pkdb'
require 'nokogiri'
require 'set'

db = PKDB.new("data_all_mag.db")

db.createTables()
vuz="ИТМО"
db.cleanTablesIfNeeded(vuz)


files = Dir.glob("ifmo/*.04.*.html") 

specs=SortedSet.new

db.prepare()

for f in files do

dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

num=1
numOrig=1
specID='?'

doc = Nokogiri::HTML(s)

skipf=false
doc.xpath("//table").each do |elem|
  elem.xpath("./tr").each do |row|
    td1 = row.xpath("./td[1]/text()").to_s
    if td1.include?('аправление')
       break if td1.include?('онтракт')
       break if td1.include?('аочн')
       elem.text.scan(/(\d\d\.\d\d\.\d\d)[\s-]+([«].*?[»\)])/) do |specID1,specName|
       specID=specID1.strip()
       mest=1
       puts "#{specID} /#{specName}/: #{mest}"
#    exit 1
       skipf=true if db.hasDateFlag(specID, vuz, dt)
       db.addSpec(specID, "ИТМО", specName, mest, 0)
       num=1
       numOrig=1
       break
       end
    end
    name = row.xpath("./td[3]/text()").to_s
    next if name === 'Номер заявления'
    next if name =~ /^[^\s]+$/
#   e = row.xpath("./td[4]/text()").to_s
#   next if e===''
   ball = row.xpath("./td[6]/text()").to_s.to_i
   
   org = row.xpath("./td[8]/text()").to_s.include?('Да')

   next if name.nil? or name === '' or name.length<4
   name=name.gsub(/\.([А-Я])\./,'. \1.')
   name=normal(name)
   puts "#{name}: #{ball} #{org}"
   fio=toFio(name)
   db.addRow(name, fio, org, ball, "ИТМО", specID, num, 1,0,0,0,dt)
   num+=1
   next if skipf
 end
end
end
db.commit()




