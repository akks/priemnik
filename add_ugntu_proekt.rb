require_relative 'pkdb'
require 'set'
require 'nokogiri'
tm0=Time.now

db = PKDB.new("data_all.db")
db.createTables()
db.cleanTables("УГНТУ")
db.cleanTables("ЗАЧИСЛЕН-УГНТУ")

db.prepare()

q = File.open("ugntu/plan.htm")
doc = Nokogiri::HTML(q)

s = doc.xpath("//table/tbody")
#puts s

specID = ''
flag=true
s.xpath("//tr").each do |row|
 id = row.xpath("./td[1]/div/text()").to_s
 ids = row.xpath("./td[1]/div/strong/text()").to_s
# flag=false if ids =~ /\s*Филиалы\s*/
# next if flag
# break if ids =~ /.*Салават.*/
# t = c[0].element_children[0].text
next if id.nil? 
 specID = id[/^([А-Я]{2,4}[а-я]?)[,\s]*/,1]
next if specID.nil? 
 puts 'id2='+specID
 mest = row.xpath("./td[3]/div/text()").to_s
 cmest = row.xpath("./td[6]/div/text()").to_s
 mest='0' unless mest=~/\d+/
 cmest='0' unless cmest=~/\d+/
 puts "#{specID} #{mest} #{cmest}"
 db.addSpec("#{specID}", "УГНТУ", "#{specID}", mest.to_i,cmest.to_i)
end

files = Dir.glob("ugntu/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new


for f in files do


s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)
	
s1 = /<table border.*?<\/table>/m.match(s)
s2 = /<h.*?<b>[^:]*:(.*?)<\/b><\/h/m.match(s)
if s2.nil?
s2 = /<h3.*?<\/h3>/m.match(s)
specName = /<b>(.*?)<\/b>/m.match(s2[0])
next if specName.nil?
specName=specName[1]
puts 'OLD:'+specName
else
specName=s2[1]
puts 'NEW:'+specName
end
#plan = /План приема - (\d+)/.match(s)
#cel = /целевой прием - (\d+)\,/.match(s2[0])

next if s1.nil? or  s2.nil? 
dt = /<h4>\s*(.*?)\s*: всего/.match(s)[1]
#or dt.nil? or plan.nil? or cel.nil?

print "File #{f} : #{dt}\n" 

#specName = specName[1]
#specName[/\s+\(г\. Уфа\)/]='' 
#specName+='-П/Б' if f =~ /Прикладной/
#specName+='-спец.' if f =~ /Специалитет/
origNum = s1[0].scan(/>оригинал</).size()
copyNum = s1[0].scan(/>копия</).size() 
totalNum = origNum+copyNum

#celPlan = cel[1].to_i
#totalPlan = plan[1].to_i
#totalPlan = 1 if totalPlan ==0

spec= specName

specs.add(spec)

#plans[spec]=totalPlan
#cels[spec]=celPlan


specID= spec[/\b([А-Я]{2,4}[а-я]?)\b/,1]

puts specID
#/\d\d\.\d\d\.\d\d/.match(spec)[0]
#specID+= 'п' if spec =~ /.*П\/Б.*/

db.addSpec(specID, "УГНТУ", spec, 1, 1)
db.addSpec(specID, "ЗАЧИСЛЕН-УГНТУ", spec, 1, 1)
#db.exec("UPDATE SPECS SET specName='#{spec}' WHERE vuz='УГНТУ' and spec='#{specID}'")
#<tr ><td align="center">2. </td>
#<td align="left" >Сакаев Р. И</td>
#<td >бюджет</td>
#<td align="center" ></td>
#<td >копия</td>
#<td >ИЕ-0;МЕ-0;РЯЕ-0;</td>
#<td style="width:75px;"></td>
#</tr>

i=1
s1[0].scan(/<tr.*?<td.*?>(.*?)\.\s*<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>.*?<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>.*?<\/td>.*?<td.*?>(.*?)<\/td>.*?<td.*?>(.*?)<\/td>/m) do 
   |row| 
#puts "---"
#puts row.to_s
   num=row[0]
   name=row[1]
   bud=row[2] 
   orig=row[3]
   balls=row[4]
#        МЕ-50;РЯЕ-78;ФЕ-45;
	ball=0
	i=0
	bs=[0,0,0]
        balls.scan(/\d+/) { |b|
	ball+=b.to_i
	bs[i]=b
	i+=1
        }
	name=normal(name)
	fio=toFio(name)
	numOr=num
	if row[5].include?('зачислен')
	of=true
  	   db.addRow(name, fio, of, ball.to_i, "ЗАЧИСЛЕН-УГНТУ", specID, num, i,bs[0],bs[1],bs[2],dt)
	else
	of=false
	    db.addRow(name, fio, of, ball.to_i, "УГНТУ", specID, num, i,bs[0],bs[1],bs[2],dt)
	end
	i=i+1 if of
	end

end


db.commit()
puts Time.now-tm0
