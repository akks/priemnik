﻿require_relative 'pkutils'
require 'watir'
require 'headless'


def saveHtml(txt,spec)

spec=spec[0..80]+spec[-20..-1] if spec.length>100
spec.gsub!(/\//,'-')

dt = Time.now.strftime("%Y.%m.%d.%H")
filename = getFileName("kfu",spec,dt)


puts "#{spec}: #{dt}"
puts "Saving html to #{filename}"

File.open(filename, 'wb') {|f| 
f.write "<html><head><title>#{spec}</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write "<h2>#{spec} : #{dt}</h2>"
f.write txt[/<table.*<\/table>/m]
f.write "</body></html>\n"
}
end

Headless.ly do
b = Watir::Browser.new :firefox
b.goto 'http://abiturient.kpfu.ru/entrant/abit_entrant_originals_list' 
#b.select_list(:name, "filial").select_value('0')
#b.select_list(:name, "level").selecte('1')
#b.select_list(:name, "finance").select_value('1')
opts=[]
inst_sel = b.select_list(:name, "p_faculty")
inst_sel.options.map(&:text).each { |inst|
	next if inst == 0
	inst_sel = b.select_list(:name, "p_faculty")
	inst_sel.select(inst)
	puts "Selected #{inst}"
	sleep(2)

	spec_sel = b.select_list(:name, "p_speciality")
	opts2 = spec_sel.options.map(&:text).to_a
	opts2.each do |k|
		#next if k =~/магистратура/ or k===''
		spec_sel = b.select_list(:name, "p_speciality")
		spec_sel.select(k)
	sleep(2)
		
		forma_sel = b.select_list(:name, "p_typeofstudy")
		if not forma_sel.options.map(&:text).include?('Очная')
			puts "#{k}: нет очной формы"
			next
		end	
		forma_sel.select 'Очная'
		sleep(2)
		
		vid_sel = b.select_list(:name, "p_category")
		if not vid_sel.options.map(&:text).include?('Бюджет')
			puts "#{k}: нет бюджета"
			next
		end
		begin
			vid_sel.select('Бюджет')
			sleep(0.5)
			while b.html.include?('Необходимо выбрать')
			    print '..'
			    sleep(0.5)
		        end
		puts " loaded #{inst}: #{k}"
		saveHtml(b.html, k)
		rescue
		    puts 'Some strange error'
		    next
		end
	end
}
end
