require_relative 'pkdb'
require 'set'

def fixDate(tm)
	tm.gsub(/2017\.(\d+)\.(\d+)(\.\d+)?/,'\2.\1')
end

tm0=Time.now

@dbname='data_all.db' if @dbname.nil?
@mainvuz='УГНТУ' if @mainvuz.nil?
@magistr=false if @magistr.nil?
@fio_vuzes="'БГУ','ЗАЧИСЛЕН-УГНТУ'"

db = PKDB.new(@dbname)
db.prepare(false)

specs = Hash.new
mesta = Hash.new

db.execQuery("SELECT spec,vuz,specName,mest FROM specs").each {|row|
sn = row[2].gsub(/\d\d\.\d\d\.\d\d[^$:]/,'')
specid = row[2][/\d\d\.\d\d\.\d\d/]
#sn=sn.gsub(/([А-Яа-я]{2})[А-Яа-я\-]*/,'\1').gsub(/[\d\s\.]/,'')
if row[1] != @mainvuz 
  sn="#{row[1]}.#{sn}"
  specs[[row[0],row[1]]]=sn
else
  specs[[row[0],row[1]]]="<a href='#{specid}.html'>#{sn}</a>"
end
mesta[[row[0],row[1]]]=row[3]
}

oldfio=''
slist=''
lastrow=nil
zcount=0
specList=Hash.new
unos=Hash.new

puts "Ищем данные в базе..."

dtmax = '2016.08.05.00'

puts "Формируем списки специальностей..."
db.execQuery("SELECT name, ball, vuz, spec, orig, num, numOr FROM records WHERE tm='#{dtmax}' and vuz='#{@mainvuz}' order by name").each do |name,ball,vuz,spec,orig,num,numOr|
	slist = specs[[spec,vuz]]
	slist += " #{num},#{numOr}/#{mesta[[spec,vuz]].to_s}" if mesta[[spec,vuz]]
	slist += "!!" if orig=="true"
	slist += "!" if orig=="true" and vuz===@mainvuz
	slist += "<br/>"
	if specList.key?(name)
		specList[name]+=slist
	else
		specList[name]=slist
	end
end

prefix=''
["select distinct(r1.name),r1.ball,r2.vuz,r2.spec,r2.orig,r2.num,r2.numOr,r2.tm
from records as r1 left join records as r2 on r1.name=r2.name
where r1.vuz='#{@mainvuz}' and r1.tm like '#{dtmax}' and not r2.vuz in('#{@mainvuz}',#{@fio_vuzes}) order by r1.name,r2.vuz,r2.spec,r2.tm
","select distinct(r1.name),r1.ball,r2.vuz,r2.spec,r2.orig,r2.num,r2.numOr,r2.tm
from records as r1 left join records as r2 on r1.fio=r2.fio
where r1.vuz='#{@mainvuz}' and r1.tm like '#{dtmax}' and r2.vuz in(#{@fio_vuzes}) order by r1.name,r2.vuz,r2.spec,r2.tm
"].each do |sql|

puts sql

vuzPrev=nil
specPrev=nil
namePrev=nil
ballPrev=nil
tmPrev=nil
first_orig_date=nil
first_date=nil
last_orig_date=nil


db.execQuery(sql).each do |name,ball,vuz,spec,orig,num,numOr,tm|
	if not (vuz===vuzPrev and spec===specPrev and name===namePrev)
	#Пошла новая строка специальности. В ней будет несколько дат с местами 
		if namePrev
			fin=''
			fin << "оригинал c #{fixDate(first_orig_date)}" if first_orig_date
			fin << " по #{fixDate(last_orig_date)}" if last_orig_date and last_orig_date!=tmPrev
			fin << "<br/>"
			specList[namePrev]+=fin
			if first_orig_date
			if first_orig_date===first_date
				unos[namePrev]="#{ballPrev} </td><td>оригинал на</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" if first_orig_date
			else
				unos[namePrev]="#{ballPrev} </td><td>унес оригинал</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" 
			end
			end
		end
		slist = prefix+(specs[[spec,vuz]]||'') +' '
		first_orig_date=nil
		last_orig_date=nil
		first_date=nil
	else
		slist = ''
	end
	if mesta[[spec,vuz]]
		mestStr = "#{num},#{numOr}/#{mesta[[spec,vuz]].to_s}:"
	   	slist << mestStr unless mestStr==='1,1/1:'
	end
	if tm===''
		slist << "?"
	else
		slist << "#{fixDate(tm)}"  # форматируем дату
	end
	if orig=="true"
		slist << "<b>!!</b>"
		first_orig_date=tm if first_orig_date.nil?
		last_orig_date=tm
	end
	first_date=tm if first_date.nil?

	slist << ";"
	specList[name] += slist
	tmPrev=tm
	vuzPrev=vuz
	specPrev=spec
	namePrev=name
	ballPrev=ball
end
if namePrev
	fin=''
	fin << "оригинал c #{fixDate(first_orig_date)}" if first_orig_date
	fin << " по #{fixDate(last_orig_date)}" if last_orig_date and last_orig_date!=tmPrev
	fin << "<br/>"
	specList[namePrev]+=fin 
	if first_orig_date
		if first_orig_date===first_date
			unos[namePrev]="#{ballPrev} </td><td>оригинал на</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" if first_orig_date
		else
			unos[namePrev]="#{ballPrev} </td><td>унес оригинал</td><td> #{specs[[specPrev,vuzPrev]]}</td><td>#{fixDate(first_orig_date)}" 
		end
	end

end
prefix='~'
end

specs1=['БМА', 'ББП']
#specs1=['02.03.01']

if @magistr 
specs=specsm
fname='indexm.html'
else
specs=specs1
fname='index.html'
end

db.saveSpecTable(fname, specs, @mainvuz)

for s in specs 
db.saveSpecHtml(s, @mainvuz, specList, dtmax, unos)
end

puts Time.now-tm0
