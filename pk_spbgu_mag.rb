﻿require_relative 'pkutils'

dt = Time.now.strftime("%Y.%m.%d.%H")

url='https://cabinet.spbu.ru/Lists/Mag_EntryLists/'
s = httpToFile(url, 'spbgu/magspecs.htm')
require 'nokogiri'
doc = Nokogiri::HTML(s)

spec = ""
url = ""

specs = {}
doc.xpath("/html/body/div/ul/li[1]/ul/li/ul/li/ul/li/ul/li/div/p/a").each do |r|
 url = 'https://cabinet.spbu.ru/Lists/Mag_EntryLists/' + r.attr('href')
 next unless url.include?('.html')
 spec = r.text.gsub('/','')
 fname = getFileName("spbgu",spec,dt)
 while specs.include?(spec)
    spec=spec+"X"
 end
 specs[spec]=url
 httpToFile(url, fname)
end

