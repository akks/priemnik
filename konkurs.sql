CREATE VIEW if not exists origs_copies AS
    SELECT specs.specName,
           specs.vuz,
           specs.mest AS MEST,
           count(1) AS TOTAL,
           sum(orig = 'true') AS ORIGS,
           100 * sum(orig = 'true') / count(1) AS [ORIG%],
           100 * sum(orig = 'true') / specs.mest AS KON_ORIG,
           100 * count(1) / specs.mest AS KONKURS
      FROM specs
           LEFT JOIN
           records ON specs.spec = records.spec AND 
                      specs.vuz = records.vuz
     GROUP BY specs.spec,
              specs.vuz;

CREATE VIEW if not exists specs_origs_usatu AS
    SELECT specs.specName,
           specs.vuz,
           specs.mest AS MEST,
           count(1) AS TOTAL,
           sum(orig = 'true') AS ORIGS,
           100 * sum(orig = 'true') / count(1) AS [ORIG%],
           100 * sum(orig = 'true') / specs.mest AS KON_ORIG,
           100 * count(1) / specs.mest AS KONKURS
      FROM specs
           LEFT JOIN
           records ON specs.spec = records.spec AND 
                      specs.vuz = records.vuz
     WHERE specs.vuz = '�����'
     GROUP BY specs.spec,
              specs.vuz;
