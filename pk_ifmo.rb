﻿require_relative 'pkutils'
require 'nokogiri'


Dir.mkdir('ifmo') unless Dir.exist?('ifmo') 
f1 = httpToFile('https://abit.itmo.ru/master/rating_rank/','ifmo/magistr.htm')
f2 = httpToFile('https://abit.itmo.ru/bachelor/rating_rank/all/','ifmo/bakalavr.htm')


dt = Time.now.strftime("%Y.%m.%d.%H")

[f1,f2].each do |q|
doc = Nokogiri::HTML(q)

s = doc.xpath("//a|//h2")

addrs = Hash.new

s.each do |a|
	puts a
	break if a.name=='h2' and a.text.include?('аочн')
	next if a.name=='h2' and a.text.include?('Очная')
	addr = a.attr('href')
	spec=a.text
	next unless addr.include?('rating_rank')
	puts "#{spec}--#{addr}"
	fname = getFileName("ifmo",spec,dt)
	httpToFile(URI.encode('https://abit.itmo.ru'+addr), fname)
end

end
