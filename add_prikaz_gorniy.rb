﻿require_relative 'pkutils'
require_relative 'pkdb'
require 'strscan'
require 'set'

vuz="ЗАЧИСЛЕН-ГОРНЫЙ"

db = PKDB.new("data_all.db")
db.createTables()
db.cleanTables(vuz)

db.prepare()
files = Dir.glob("gorniy_p/*.txt") 

specs=SortedSet.new


for f in files do
print "File #{f}\n" 

s=IO.read(f)
s.force_encoding('UTF-8')
fixE(s)	   

sc = StringScanner.new(s)

numOrig=1
num=1

spec=''
specID=''
skip=false

loop do

break if sc.scan_until(/((\d\d\.\d\d\.\d\d).*?(.+))|(([А-Яа-я\-]+?\s[^\n]+?)(19|20)\d\d)/).nil?

if sc[1]
#puts "~~~#{sc[1]}~~~"
	specID = sc[2]
	spec = sc[3]
	db.addSpec(specID, vuz, spec, 1, 1)
	numOrig=1
	num=1
	puts "Spec: #{specID}: #{spec}"
	skip=false
#	exit
else
	nm = sc[5]
	ball = 0
next unless nm
	name=normal(nm)
next if name===''
puts name
	fio=toFio(name)
	db.addRow(name, fio, true, ball, vuz, specID, num,numOrig,0,0,0)
	num+=1
end

break if sc.eos?

end
end


db.commit()
