require_relative 'pkdb'
require 'nokogiri'
require 'set'


vuz="СПБГТУ"
initDBs(vuz)

files = Dir.glob("spbstu/*.html") 

specs=SortedSet.new

plans=Hash.new
cels=Hash.new

for f in files do

dt = getDateFromName(f)
print "File #{f} : #{dt} " 

dbc = @db
mag = false

if f.include?('-mag')
   mag = true
   dbc = @dbm
end

s = IO.read(f)
s.force_encoding('UTF-8')
fixE(s)


doc = Nokogiri::HTML(s)

spec = doc.xpath("/html/head/title/text()").text
next if spec===''
specID = f[/\-(\d\d\.\d\d\.\d\d)/,1]
specID = spec if specID.nil?
while specs.include?(specID)
specID+="'"
end

dbc.addSpec(specID, "СПБГТУ", spec, 1, 1)
next if dbc.hasDateFlag(specID, vuz, dt)

tb = doc.xpath("//table/tbody")

ball = 0
b1 = 0
b2 = 0
b3 = 0
n=0
tb.xpath("./tr").each do |row|
 if mag
   name = row.xpath("./td[2]/text()").to_s
   ball = row.xpath("./td[4]/text()").to_s.to_i
   orig = row.xpath("./td[4]/text()").to_s.include?('риг')
   puts name if orig
 else
   name = row.xpath("./td[2]/text()").to_s
   ball = row.xpath("./td[4]/text()").to_s.to_i
   b1 = row.xpath("./td[5]/text()").to_s.to_i
   b2 = row.xpath("./td[6]/text()").to_s.to_i
   b3 = row.xpath("./td[7]/text()").to_s.to_i
   orig = row.xpath("./td[10]/text()").to_s.include?('Да')
 end
 next if name.nil? or name === ''
# puts name
 name=normal(name)
 fio=toFio(name)

 dbc.addRow(name, fio, orig, ball, "СПБГТУ", specID, 1, 1,b1,b2,b3,dt)
#numOrig+=1 if orig==="оригинал"
#num+=1
 n+=1
end
puts "#{n} заявлений"

end

commitDBs()