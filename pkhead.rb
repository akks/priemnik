require 'headless'

MaybeHeadless = nil
if ARGV[0].nil?
  def MaybeHeadless(&b)
    Headless.ly(&b)
  end 
else
 def MaybeHeadless(&b)
   b.call()
 end    
end
