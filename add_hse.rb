require_relative 'pkdb'
require 'set'
require 'nokogiri'

vuz="ВШЭ"
initDBs(vuz)

q = File.open("hse/hse_kolmest.htm")
doc = Nokogiri::HTML(q)

s = doc.xpath("//table/tbody")
#puts s

specID = ''
s.xpath("//tr").each do |row|
 c = row.element_children

 id = c[0].text[/\d\d\.\d\d\.\d\d/]
 specID = id if id

 next if c.count<2
 t = c[0].element_children[0].text
 mest = c[1].text
 mest = mest.to_i
 puts "#{specID} #{t} -- #{mest}" if specID != ''
 @db.addSpec("#{specID} #{t}", vuz, "#{specID} #{t}", mest,1) if specID != ''
end
files = Dir.glob("hse/*.csv") 

specs=SortedSet.new


for f in files do

print "File #{f}: " 

s= IO.read(f, :encoding => 'windows-1251')
s=s.encode('UTF-8')
s.force_encoding('UTF-8')

dt = getDateFromName(f)
stamps = @db.allSpecDates(vuz)

#s.split(/Список Абитуриентов/).each do |s1|


#Направление 38.03.05 Бизнес-информатика;;;;;;;;;;;;;;;;;;;;
#Образовательная программа "Бизнес-информатика";;;;;;;;;;;;;;;;;;;;
#очная форма обучения;;;;;;;;;;;;;;;;;;;;


napr = /(Направление|Специальность) (\d.*),/.match(s)
prog = /Образовательная программа ""(.*?)""",/.match(s)
form = /(очная|заочная) форма обучения,/.match(s)

if napr.nil? or prog.nil? or form.nil? 
puts "Ошибка в файле #{f}"
next
end

next if form[1] =~ /.*заочная.*/

specID = /\d\d\.\d\d\.\d\d/.match(napr[2])[0]
spec = specID +' '+prog[1]
specID=spec

next if @db.hasDateFlag(specID, vuz, dt)


i=1
n=0
s.scan(/^(.*?),.*?,\s*(.*?)\s*,(.*?),.*?,.*?,.*?,.*?,.*?,.*?,.*?,.*?,.*?,(.*?),/) do |num,name,orig,ball|#
	name=normal(name)
	fio=toFio(name)
	@db.addRow(name, fio, orig==="Подлинник", ball, vuz, specID, num,i,0,0,0,dt)

#	fio=fio.sub(/\s+$/,'')
#	db.addRow(fio, fio, orig==='Да', ball, "БГУ", specID, i,1)
	i=i+1 if orig==="Подлинник"
    n=n+1
end
puts "#{n} заявлений"
#end

end

commitDBs()





