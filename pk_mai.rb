﻿require_relative 'pkutils'
require 'watir'
require 'headless'


def saveHtml(txt,spec)

m = /(\d\d)\.(\d\d)\.(\d\d\d\d)/.match(txt)
dt=''
dt = "#{m[3]}.#{m[2]}.#{m[1]}" unless m.nil?

filename = getFileName("mai",spec,dt)

specID = spec[/\d\d\.\d\d\.\d\d/]
spec.gsub!(/\d\d\.\d\d\.\d\d\s*/,'')

puts "#{specID}: #{spec} #{dt}"
puts "Saving html to #{filename}"

File.open(filename, 'wb') {|f| 
#	f.write(txt)
f.write "<html><head><title>#{spec}</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write txt[/<div id="tab".*?<\/div>/m]
f.write "</body></html>\n"
}
end


Dir.mkdir('mai') unless Dir.exist?('mai')


Headless.ly do

b = Watir::Browser.new :firefox
b.goto 'http://mai.ru/priem/list/' 

sleep(1)
b.select_list(:id, "place").select('МАИ')
#b.select_list(:name, "level").select_value('1')
#b.select_list(:name, "finance").select_value('1')

level_sel= b.select_list(:id, "level_select")
level_sel.options.each { |level|
	next if level.text.length<5
	puts level.text
	level_sel.select(level.text)

	spec_sel = b.select_list(:id, "spec_select")
	opts = spec_sel.options.map(&:text).to_a
	puts opts
	opts.each { |spc|
		next if spc.length<5
		spec_sel.select(spc)

		forma_sel = b.select_list(:id, "form_select")
		next if not forma_sel.options.map(&:text).include?('очная')
		forma_sel.select('очная')

		osn_sel = b.select_list(:id, "pay_select")
		next if not osn_sel.options.map(&:text).include?('Бюджет')
		osn_sel.select('Бюджет')
		sleep(0.5)
		s=b.html
		puts "Saving html: #{s.length} char"
                saveHtml(s, spc)

	}
}
end