require 'sqlite3'
require 'spreadsheet'
require 'set'
require_relative 'pkdb'

class UGATUParser
  
  def initialize(mag)
    @magistr = mag
  end

  def readHeaders(f)
    puts f
    s = IO.read(f)
    s.force_encoding('UTF-8')
    @table = /<table.*?<\/table>/m.match(s)
    @header = /<h4>.*?<span>.*?<\/div>.*?<\/div>.*?<\/div>/m.match(s)
    specName = /(\d\d\.\d\d\.\d\d\s.*?)\-[БСМИКMBS]+\.html/m.match(f)[1].strip
    puts specName
    dt = getDateFromName(f).gsub(/\-/,'.')
    plan = /(План приема|Вакантные места) - (\d+)/.match(s)
    if @header.nil?
        puts 'Header not found!'
    	return
    end
    if @table.nil?
        puts 'table not found!'
        exit 1
    	return
    end
    @header=@header[0]

    cel = /целевой прием - (\d+)\,/.match(@header)
    # return if @table.nil? or @specName.nil? or dt.nil? or plan.nil?

    @table = @table[0]
    #specName.gsub!(/\s+\(Бюджет\)/,'')
    #specName += '-спец.' if f =~ /Специалитет/

    if cel.nil?
      @celPlan = 0
    else
      @celPlan = cel[1].to_i
    end

    if plan
      @totalPlan = plan[2].to_i
			@totalPlan = 1 if @totalPlan ==0
    end

		@dateStr = dt
		
    @spec=specName
    @specID=/\d\d\.\d\d\.\d\d/.match(@spec)[0]
    @specID+= 'И' if @spec =~ /.*Ишимбай.*/
    @specID+= 'К' if @spec =~ /.*Кумертау.*/
    @specID+= 'к' if @spec =~ /.*Контракт.*/
  end

  def findSpec()
    yield @specID,@spec,@totalPlan,@celPlan,@dateStr
  end

  def forAllRecords()
    if @magistr
    found=0
#    @table.scan(/<tr[^<]*?<td>\d+<\/td>[^<]*?<td>(\d+)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(Да|Нет)/m) do |num,name,ball,b1,b2,orig|
    @table.scan(/<tr[^<]*?<td>(\d+)<\/td>[^<]*?<td><a[^>]+>(.*?)<\/a>[^<]*?<\/td>[^<]*?<td>(.*?)<\/td>[^<]*?<td>(.*?)<\/td>[^<]*?<td>(.*?)<\/td>[^<]*?<td>(Да|Нет)/m) do |num,name,ball,b1,b2,orig|
      yield num,name.gsub(/\s*<.*?>\s*/,''),ball.to_i,0,b1,b2,orig === 'Да'
      found=1
    end

	if found==0
        regNormal = /<tr.*?>.*?<td>\d+<\/td>.*?<td>(\d+)<\/td>.*?<td>.*?>(.*?)<\/a>.*?<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(Да|Нет)/m
        @table.scan(regNormal) do |num,name,ball,b1,b3,b2,orig|
				  yield num,name.gsub(/\s*<.*?>\s*/,''),ball.to_i,b1,b2,b3,orig === 'Да'
        end
        end

    else
      if @spec =~ /.*Контракт.*/
        @table.scan(/<tr.*?>.*?<td>\d+<\/td>.*?<td>(\d+)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(Да|Нет)/m) do |num,name,ball,b1,b3,b2,orig|
          yield num,name.gsub(/\s*<.*?>\s*/,''),ball.to_i,b1,b2,b3,orig === 'Да'
        end
			else
        regNormal = /<tr[^<]*?[^<]*?<td>(\d+)<\/td>.*?<td>.*?>(.*?)<\/a>.*?<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(Да|Нет)/m
        found=0
		@table.scan(regNormal) do |num,name,ball,b1,b3,b2,orig|
				  yield num,name.gsub(/\s*<.*?>\s*/,''),ball.to_i,b1,b2,b3,orig === 'Да'
				  found=1
        end

		if found==0
        regNormal = /<tr.*?>.*?<td>\d+<\/td>.*?<td>(\d+)<\/td>.*?<td>.*?>(.*?)<\/a>.*?<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(Да|Нет)/m
        @table.scan(regNormal) do |num,name,ball,b1,b3,b2,orig|
				  yield num,name.gsub(/\s*<.*?>\s*/,''),ball.to_i,b1,b2,b3,orig === 'Да'
        end
        end

				
				
      end
    end
  end
end
