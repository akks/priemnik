﻿require_relative 'mod_usatu'
require 'set'

@magistr=false if @magistr.nil?

if @magistr

files = Dir.glob("magistr/*.html") 
stats = File.open('stats_magistr.csv', 'w:cp1251') 
konkurs = File.open('konkurs_magistr.csv', 'w:cp1251')
statsO = File.open('stats_orig_magistr.csv', 'w:cp1251') 
konkursO = File.open('konkurs_orig_magistr.csv', 'w:cp1251')

else

files = Dir.glob("bakalavr/*.html") 

stats = File.open('stats.csv', 'w:cp1251') 
konkurs = File.open('konkurs.csv', 'w:cp1251')
statsO = File.open('stats_orig.csv', 'w:cp1251') 
konkursO = File.open('konkurs_orig.csv', 'w:cp1251')

end

counts=Hash.new
konkursVals=Hash.new
countsO=Hash.new
konkursValsO=Hash.new

dates=SortedSet.new
specs=SortedSet.new

plans=Hash.new
cels=Hash.new

specFullName=Hash.new

total=Hash.new
totalO=Hash.new

totalGr=Hash.new
totalGrO=Hash.new

p = UGATUParser.new(@magistr)

for f in files do

	print "File #{f} \n" 

	p.readHeaders(f)

	p.findSpec() do |specID, spec, totalPlan, celPlan, dateStr|
		dates.add(dateStr)
		specs.add(spec)
		specFullName[specID]=spec
		plans[spec] = totalPlan
		cels[spec] = celPlan
		
		totalNum = 0
		origNum = 0
		p.forAllRecords() do |num,name,ball,b1,b3,b2,orig|
			totalNum += 1
			origNum += 1 if orig
		end
		
		puts "#{dateStr} #{spec} #{totalNum}"
		
		counts[[dateStr,spec]]=totalNum
		konkursVals[[dateStr,spec]]='%.2f'% (1.0*totalNum/totalPlan)

		countsO[[dateStr,spec]]=origNum
		konkursValsO[[dateStr,spec]]='%.2f'% (1.0*origNum/totalPlan)
	end

end

stats.write "специальность;м;ц"
konkurs.write "специальность;м;ц"
statsO.write "специальность;м;ц"
konkursO.write "специальность;м;ц"


dates.each { |date|
	stats.write ";#{date}"
	statsO.write ";#{date}"
	konkurs.write ";#{date}"
	konkursO.write ";#{date}"
	total[date] = 0
	totalO[date] = 0
}

stats.write "\n"
konkurs.write "\n"
statsO.write "\n"
konkursO.write "\n"

groups={'ОНФ'=>['01.03.02','01.03.04','02.03.01'], 'ФИРТ-И'=>['02.03.03','09.03.01','09.03.02','09.03.03','09.03.04','09.05.01','10.03.01','10.05.05']}

groups.values.each { |v|
 dates.each { |date|
   totalGr[[v,date]] = 0
   totalGrO[[v,date]] = 0
 }
}

specs.each { |spec|
	stats.write "#{spec};#{plans[spec].to_s};#{cels[spec].to_s}"
	konkurs.write "#{spec};#{plans[spec].to_s};#{cels[spec].to_s}"
	statsO.write "#{spec};#{plans[spec].to_s};#{cels[spec].to_s}"
	konkursO.write "#{spec};#{plans[spec].to_s};#{cels[spec].to_s}"

	dates.each { |date|
		v = counts[[date,spec]]
		v='0' if v.nil? 
		stats.write ";#{v}"
		total[date] += v.to_i
	
		v = konkursVals[[date,spec]]
		v='0.0' if v.nil? 
		v['.']=','
		konkurs.write ";#{v}"

		v = countsO[[date,spec]]
		v='0' if v.nil? 
		statsO.write ";#{v}"
		totalO[date] += v.to_i

		v = konkursValsO[[date,spec]]
		v='0.0' if v.nil? 
		v['.']=','
		konkursO.write ";#{v}"
	}
	stats.write "\n"
	konkurs.write "\n"
	statsO.write "\n"
	konkursO.write "\n"
}



stats.write ";;"
statsO.write ";;"
dates.each { |date|
	stats.write ";#{total[date]}"
	statsO.write ";#{totalO[date]}"
}

stats.write "\n"
statsO.write "\n"

groups.each { |gr,spcs|

stats.write ";"
statsO.write ";"

stats.write gr
statsO.write gr

stats.write ";"
statsO.write ";"


dates.each { |date|
 totalGr[[date,gr]] = 0
 totalGrO[[date,gr]] = 0
 spcs.each { |specID|
   spec = specFullName[specID]
   totalGr[[date,gr]] += counts[[date,spec]].to_i
   totalGrO[[date,gr]] += countsO[[date,spec]].to_i
 }
 stats.write ";#{totalGr[[date,gr]]}"
 statsO.write ";#{totalGr[[date,gr]]}"
}
stats.write "\n"
statsO.write "\n"


}


stats.close()
konkurs.close()
statsO.close()
konkursO.close()
