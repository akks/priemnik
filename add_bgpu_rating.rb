require_relative 'pkdb'
require 'set'

db = PKDB.new("data_all.db")

db.createTables()
db.cleanTables("БГПУ")

dbm = PKDB.new("data_all_mag.db")
dbm.createTables()
dbm.cleanTables("БГПУ")


files = Dir.glob("bgpu_r/*.txt") 

specs=SortedSet.new

db.prepare()
dbm.prepare()

for f in files do


	print "File #{f} \n" 
	dt = getDateFromName(f).gsub('-','.')

	s1= IO.read(f)
	s1.force_encoding('UTF-8')
	fixE(s1)

	fak = /Факультет:\s*(.*?)$/.match(s1)
	spec = /Направление:\s*(.*?)$/.match(s1)
	form = /Форма обучения:\s*(.*?)$/.match(s1)
	
	if fak.nil? or spec.nil? or form.nil? 
	#	puts s1
		next
	end
	#next if spec[1] =~ /.*Магистратура.*/
	next if form[1] =~ /.*заочная.*/

	puts fak[1]
	puts spec[1]

	#;СПЕЦИАЛЬНОСТЬ:;;Магистратура по напр. проф. обр. программа IT-менеджмент в государственном муниципальном образовании;;;;;;;;;;
	#;ФОРМА ОБУЧЕНИЯ:;;заочная;;;;;;;;;;

	if spec[1] =~ /.*Магистратура.*/
	 dbm.addSpec(spec[1], "БГПУ", spec[1], 1,1)
	else
	 db.addSpec(spec[1], "БГПУ", spec[1], 1,1)
	end
	#27;Антонова Анастасия Сергеевна;;;;;
	numOrig=1
	num=1
	# 1;Дмитриева Ольга Вячеславовна;;;;; ; ;Да; ; ;Да;Оригинал;
#         8;Басырова Ралия Рависовна;;;;;Да; ; ;172.0; ; ;Да;Нет;Копия;
	s1.scan(/([А-Я][А-Яа-я]+ [А-Я][А-Яа-я]+ [А-Я][А-Яа-я]+).*?(Да|Нет)\s*\n/m) do |name,orig|
#		puts name+' '+orig
		name=normal(name)
		fio=toFio(name)
		
		if spec[1] =~ /.*Магистратура.*/
		  dbm.addRow(name, fio, orig==="Да", 0, "БГПУ", spec[1], num,numOrig,0,0,0,dt)
		else
		  db.addRow(name, fio, orig==="Да", 0, "БГПУ", spec[1], num,numOrig,0,0,0,dt)
		end
		numOrig+=1 if orig==="Да"
		num+=1

	end

	

end

db.commit()
dbm.commit()
