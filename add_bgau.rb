require_relative 'pkdb'
require 'nokogiri'
require 'set'

vuz="БГАУ"
files = Dir.glob("bgau/*.html") 
specs=Set.new

initDBs(vuz)

dbc=@db
for f in files do
	mag=false
	orIdx = 13
	minSize = 16
	if f.include?('_Маг_')
	  dbc = @dbm
	  mag = true
	  orIdx = 8
	  minSize = 11
	else
	  dbc = @db
	end

	next if f=~/возмещение/
	dt = getDateFromName(f)
	print "File #{f} : #{dt}\n" 
	
	s= IO.read(f)
	s.force_encoding('UTF-8')
	fixE(s)

	num=1
	numOrig=1
#	Направление подготовки/специальность - 35.06.02 Лесное хозяйство
	sm = /Направление подготовки.*?(\d\d\.\d\d\.\d\d)\s+(.*?)\s*</.match(s)
	specID=sm[1]
	spec=sm[2]
#	while specs.include?(specID)
	specID+="з" if f =~ /Заоч/
	specID+="ц" if f =~ /Цел/
#	end
	specs.add(specID)

	mest=s[/Всего мест.*?(\d+)/,1].to_i
	puts "#{specID} #{mest} #{spec}"

	dbc.addSpec(specID, vuz, spec, mest, 0)
	next if dbc.hasDateFlag(specID, vuz, dt)

	doc = Nokogiri::HTML(s)
	tb = doc.xpath("//table")
	tb.xpath("./tr").each do |row|

		p=row.css("td")
		next if p.size<minSize
		
		#puts p.size
		name = p[2].text
		
		orig =  !(p[14].text.empty?)
		ball = p[15].text
		fio=name

		next if name.nil? or name === '' or name.include? 'Физическое лицо'
		
		#name=normal(name)
		#fio=toFio(name)
		
		
		#puts "#{num}/#{numOrig} #{name} :#{ball} #{orig}" #if mag
		dbc.addRow(name, fio, orig, ball, vuz, specID, num, numOrig, 0,0,0,dt)
		numOrig+=1 if orig
		num+=1
	end
	puts "#{specID} [#{spec}], #{mest} мест, #{num} заявлений"

end

commitDBs()
