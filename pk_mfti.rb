﻿# urls=[
# 'https://pk.mipt.ru/bachelor/list/?ajax=Y&method=getList'
# ]

# dt = Time.now.strftime("%Y.%m.%d.%H")

# urls.each do |addr|

# s = httpToString(addr)
# fixE(s)

# s1 = /<table.*?<\/table>/m.match(s)

# next if s1.nil?

# origNum = s1[0].scan(/>оригинал</).size()
# copyNum = s1[0].scan(/>копия</).size() 
# totalNum = origNum+copyNum

# puts origNum.to_s+'/'+totalNum.to_s

# fname=getFileName("fizteh", "МФТИ", dt)

# File.open(fname, 'w') {|f| 
# f.write "<html><head><title>"+origNum.to_s+"</title>"
# f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
# f.write "</head><body>"
# f.write "<h4> Всего "+totalNum.to_s+", оригиналов "+ origNum.to_s+", копий "+copyNum.to_s+"</h4>"
# f.write s1[0]
# f.write "</body></html>\n"
# }

# end

require_relative 'pkutils'
require 'watir'

require_relative 'pkhead'
MaybeHeadless do

b = Watir::Browser.new :firefox
b.goto 'https://pk.mipt.ru/bachelor/list/' 

dt = Time.now.strftime("%Y.%m.%d.%H")

#sleep(1)
b.select_list(:class, "form-control").select('Общий конкурс')

divs = b.divs(:class, "filter-wrap")
sf = {class: "form-control"}
spec_sel= divs[1].select_list(sf)
specs = spec_sel.options.map(&:text).to_a

apply = b.divs(class: "btn")[0]

specs.each { |spec|
	next if spec.length<5
	puts spec
	spec_sel.select(spec)

	group_sel = divs[2].select_list(sf)
    groups = group_sel.options.map(&:text).to_a
	puts groups
	groups.each { |group|
		next if group.length<5
		group_sel.select(group)

		osn_sel = divs[3].select_list(sf)
		next if not osn_sel.options.map(&:text).include?('Бюджетное обучение')
		osn_sel.select('Бюджетное обучение')
        apply.click
		#sleep(0.5)
		s=b.html
		puts "Saving html: #{s.length} chars #{spec} #{group}"

        filename = getFileName("mfti",group,dt)
        File.open(filename, 'wb') {|f| 
          f.write s
        }
        
	}
}
end