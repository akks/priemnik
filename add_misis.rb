require_relative 'pkdb'
require 'set'
require 'nokogiri'

db = PKDB.new("data_all.db")
db.createTables()

vuz="МИСИС"
db.cleanTablesIfNeeded(vuz)


files = Dir.glob("misis/*.html") 

specs=SortedSet.new
plans=Hash.new
cels=Hash.new


db.prepare()


for f in files do

dt = getDateFromName(f).gsub(/\-/,'.')

print "File #{f} : #{dt}\n" 

duplDate=false
s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

doc = Nokogiri::HTML(s)
tb = doc.xpath("//table/tbody")

tb.xpath("./tr").each do |row|
 name = row.xpath("./td[3]/text()").to_s
 spcs=row.xpath("./td[4]/text()").to_s+
 	row.xpath("./td[5]/text()").to_s+
 	row.xpath("./td[6]/text()").to_s+
 	row.xpath("./td[7]/text()").to_s
 orig = row.xpath("./td[10]/text()").to_s
 next if name.nil? or name === ''
 name=normal(name)
 fio=toFio(name)
 spcs.scan(/\d\d\.\d\d\.\d\d/) do |spec|
	unless specs.include?(spec)
	   specs.add(spec.to_s)
	   db.addSpec(spec, "МИСИС", spec+':', 1,1)
	   puts "#{spec}"
	   duplDate=db.hasDateFlag(spec, vuz, dt)
	   next if duplDate
	end
	next if duplDate
	db.addRow(name, fio, orig==="+", 0, "МИСИС", spec, 1, 1,0,0,0,dt)
 end
 next if duplDate
end
next if duplDate
end

db.commit()




