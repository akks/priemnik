﻿
require 'set'
require_relative 'pkdb'



def saveDiffs(spec)

files = Dir.glob("bakalavr/*.html") 

counts=Hash.new
konkursVals=Hash.new
countsO=Hash.new
konkursValsO=Hash.new

lists=Hash.new
olists=Hash.new

dates=SortedSet.new

fios=SortedSet.new
ofios=SortedSet.new


plans=Hash.new
cels=Hash.new

for f in files do

next unless f.include?(spec)

#print "File #{f} \n" 

s= IO.read(f)

s.force_encoding('UTF-8')

s1 = /<div[^>]*table[^>]*>.*?<\/div>/m.match(s)

s2 = /<h4><span>.*?<\/p>/m.match(s)


specName = /<br>\s+(\d\d\.\d\d\.\d\d.*?) <br>/m.match(s)


dt = /по состоянию на (\d\d)\.(\d\d)\.(\d+)\s(\d+)/.match(s)

plan = /План приема - (\d+)/.match(s)
cel = /целевой прием - (\d+)\,/.match(s2[0])

next if s1.nil? or  s2.nil? or  specName.nil? or dt.nil? or plan.nil? or cel.nil?

specName = specName[1]
specName[/\s+\(Бюджет\)/]=''

#specName+='-П/Б' if f =~ /Прикладной/
specName+='-спец.' if f =~ /Специалитет/

zz= s1[0].scan(/<td>Да<\/td>[^<]*<td>[^>]*<\/td>[^<]*<!--<td><\/td>-->[^<]*<td>[^>]*<\/td>[^<]*<\/tr>/)
origNum = zz.size()
zz= s1[0].scan(/<td>Нет<\/td>[^<]*<td>[^>]*<\/td>[^<]*<!--<td><\/td>-->[^<]*<td>[^>]*<\/td>[^<]*<\/tr>/)
copyNum = zz.size()
totalNum = origNum+copyNum


celPlan = cel[1].to_i
totalPlan = plan[1].to_i
totalPlan = 1 if totalPlan ==0

fios = SortedSet.new
ofios= SortedSet.new

s1[0].scan(/<tr.*?>.*?<td>\d+<\/td>.*?<td>(\d+)<\/td>.*?<td>(.*?)<\/td>.*?<td>.*?<\/td>.*?<td>.*?<\/td>.*?<td>.*?<\/td>.*?<td>.*?<\/td>.*?<td>(Да|Нет)/m) do |num,fio,orig|
fio=normal(fio)

next if fio==='ФИО'
#puts num,fio
fios.add(fio)
ofios.add(fio) if orig==='Да'
end


#puts fios.to_a.join(',')

dateStr = "#{dt[3]}.#{dt[2]}.#{dt[1]} %02d" % dt[4]

spec= specName

dates.add(dateStr)

plans[dateStr]=totalPlan
cels[dateStr]=celPlan
lists[dateStr]=fios
olists[dateStr]=ofios

#puts dateStr
#puts spec

counts[dateStr]=totalNum
konkursVals[dateStr]='%.2f'% (1.0*totalNum/totalPlan)

countsO[dateStr]=origNum
konkursValsO[dateStr]='%.2f'% (1.0*origNum/totalPlan)

end

prevDate=nil


puts '============='+spec+'====================='
dates.each do |date|
  list2=lists[date]
  olist2=olists[date]
  if prevDate 
    list1=lists[prevDate]
    olist1=olists[prevDate]
  else
    list1=SortedSet.new    
    olist1=SortedSet.new    
  end
    puts date

    diff1 = (list2-list1)
    diff2 = (list1-list2)
    odiff1 = (olist2-olist1)
    odiff2 = (olist1-olist2)

    puts "Добавились : "+diff1.to_a.join(', ')  unless diff1.empty?
    puts "Пропали    : "+diff2.to_a.join(', ') unless diff2.empty?
    puts "Добавились О: "+odiff1.to_a.join(', ')  unless odiff1.empty?
    puts "Пропали    О: "+odiff2.to_a.join(', ') unless odiff2.empty?

  prevDate=date

end
   

end

#saveDiffs('01.03.02')
#saveDiffs('01.03.04')
#saveDiffs('02.03.01')
#saveDiffs('02.03.03')
saveDiffs('09.03.04')

