require 'net/http'
require 'uri'
require 'net/https'
require 'spreadsheet'
#require 'unicode_utils'
require 'pdf/reader' # gem install pdf-reader
require 'no_proxy_fix'

def toFio(name)
  name.sub(/([A-Яа-я]+)\s+([A-Яа-я])[A-Яа-я\-]*($|[\.\s]*([A-Яа-я])[A-Яа-я\-]*[\.\s]*)/, '\1 \2. \4.')
end

class String
  def titlecase
    split(/([[:alpha:]]+)/).map(&:capitalize).join
  end
end

def normal(name)
  name = name.strip().gsub(/\s+/, ' ').split(' ').map {|word| word.titlecase}.join(' ')
end



def httpToString(addr, encode:true, html:true, post:false, headers:{}, params:{}, debug:false, identity:false)
uri = URI.parse(addr)
http = Net::HTTP.new(uri.host, uri.port)
http.read_timeout = 300

if post
	request = Net::HTTP::Post.new(uri.request_uri)
else
	request = Net::HTTP::Get.new(uri.request_uri)
end

if html
request.add_field('Content-Type', 'application/x-www-form-urlencoded') if post and params!=0
request.add_field('Accept-Language','ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4')
#request.add_field('Accept', 'text/html')
request.add_field('User-Agent','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3128.0 Safari/537.36 OPR/48.0.2643.0 (Edition developer)')
end

headers.each { |k,v| request.add_field(k,v) }
request.set_form_data(params) unless params.empty?
if debug
  puts "Headers: #{headers}" unless headers.empty?
  puts "Params: #{params}" unless params.empty?
end

request["accept-encoding"] = "identity" if identity

if addr =~ /https/
 http.use_ssl = true
 http.verify_mode = OpenSSL::SSL::VERIFY_NONE
end

print "POST-" if post
puts "запрос на "+uri.to_s
response = http.request(request)
puts "OK запрос на "+uri.to_s
s=response.body()
s.force_encoding('UTF-8') if encode
return s
end


def httpToFile(addr, filename, encode:true, html:true, post:false, headers:{}, params:{}, debug: false, identity: false)
s=httpToString(addr, encode:encode, html:html, post:post, headers:headers, params:params, debug:debug, identity: identity)
puts "ответ получен от #{addr} сохраняем в #{filename}"
File.open(filename, 'wb') {|f|
  f.write s
}
return s
end


# credits to :
# 	https://github.com/yob/pdf-reader/blob/master/examples/text.rb

def convertPDFToText(filename,outname)
  PDF::Reader.open(filename) do |reader|
  puts "Converting : #{filename}"
  pageno = 0
  txt = reader.pages.map do |page| 
  	pageno += 1
  	begin
  		page.text 
  	rescue
  		puts "Page #{pageno}/#{reader.page_count} Failed to convert"
  		''
  	end
  end # pages map
  puts "\nWriting text to disk"
  out =  txt.join("\n")
	  fixE(out)
  File.write outname, out
  end # reader
end

def convertToCSV(filename, fixenc)
  outname = filename.sub(/(\.xls$|\.xlsx$)/,'.csv')
  puts "конвертируем в  #{outname}"
  
  book = Spreadsheet.open filename
  output = File.open(outname, "w")

  sheet1 = book.worksheet 0
  i=0
  sheet1.each do |row|
     row.each do |cell|
        s = cell.to_s
        if fixenc
        output.write s.encode("CP850").force_encoding('CP1251').encode('UTF-8') if cell 
        else
        
        if s.include?(";") 
          output.write '"'+s+'"'
        else
        output.write s
        end 
          
        end
        output.write ";"
     end
     output.write "\n"

  end

  #s1 = s.encode("CP850").force_encoding('CP1251').encode('UTF-8')
end


def getFileName(vuz,spec,date,ext="html",sep='-')
  Dir.mkdir(vuz) unless Dir.exist?(vuz) 
  return "#{vuz}/#{vuz}#{sep}#{date}#{sep}#{spec}.#{ext}"
end

def getDateFromName(fname)
  m = /[\-._](\d+\.\d+\.\d+\.\d+(\.\d+)?)[\-._]/.match(fname)
  return m[1] unless m.nil?
  m = /[\-._](\d+\.\d+\.\d+)[\-._]/.match(fname)
  return m[1] unless m.nil?
  return ''
end

def fixE(s)
	s.gsub!(/ё/,'е')
	s.gsub!(/Ё/,'Е')
end