require_relative 'pkdb'
require 'set'

def fixDate(tm)
	tm.gsub(/2020\.(\d+)\.(\d+)(\.\d+)?/,'\2.\1')
end

tm0=Time.now

@dbname='data_all.db' if @dbname.nil?
@mainvuz='УГАТУ' if @mainvuz.nil?
@magistr=false if @magistr.nil?
@fio_vuzes="'УГНТУ','БГУ','ЗАЧИСЛЕН-УГНТУ'" if @fio_vuzes.nil?
@indexFileName='index.html' if @indexFileName.nil?

db = PKDB.new(@dbname)
db.prepare(false)


oldfio=''
slist=''
lastrow=nil
zcount=0

puts "Ищем данные в базе..."


puts "Формируем списки специальностей..."

dtmax = db.maxDateVuz(@mainvuz)
specs,specIds,mesta,specList = db.getSpecListByNames(@mainvuz, dtmax)
puts "списки специальностей для каждого абитуриента загружены"

unos, nowOrig = db.getUnos(specs, specList, mesta, dtmax, @mainvuz)

#specs=specs1.sort() if specs.nil?

puts 'Специальности:',specIds,'----'
db.saveSpecTable(@indexFileName, @mainvuz)
for s in specIds
   db.saveSpecHtml(s, @mainvuz, specList, dtmax, unos, nowOrig)
end

puts Time.now-tm0
