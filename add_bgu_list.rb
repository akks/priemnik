require_relative 'pkdb'
require 'nokogiri'
require 'set'


vuz="БГУ"
files = Dir.glob("bsu/bsu*.html") 
specs=SortedSet.new

initDBs(vuz)

dbc=@db
mag=false

for f in files do

dt = getDateFromName(f)

print "File #{f} : #{dt}\n" 

s= IO.read(f)
s.force_encoding('UTF-8')
fixE(s)

num=1
numOrig=1
specID='?'
mesta=Hash.new

s.scan(/(\d\d\.\d\d\.\d\d)\s\-\s(.*?)<.*?Бюджет\s\-\s(\d+).*?(\d+)/m) do |specID1, spec, mest, cel|
  puts "#{specID1} #{spec} #{mest} #{cel}"
  specID = specID1
  
  mag=false
  if specID.match?(/\d\d\.04\.\d\d/)
    dbc = @dbm
    mag = true
  else
    dbc = @db
  end
  dbc.addSpec(specID, vuz, spec, mest, cel)
  mesta[specID]=mest
end
if specID==='?'
  puts "Error"
  next
end

next if dbc.hasDateFlag(specID, vuz, dt)


doc = Nokogiri::HTML(s)
tb = doc.xpath("//table[@class='table table-hover']/tbody")
tb.xpath("./tr").each do |row|

name = row.xpath("./td[2]/a/text()").to_s
ball = 0
org = row.xpath("./td[4]/text()").to_s
 if org=~/.*Оригинал.*/ 
   orig=true
 else
   orig=false
 end
#puts "#{name} #{ball} #{orig} #{dt}"
 next if name.nil? or name === ''
 name=normal(name)
 fio=toFio(name)

 dbc.addRow(name, fio, orig, ball, vuz, specID, num, numOrig,0,0,0,dt)
 numOrig+=1 if orig
 num+=1
end
puts "#{specID}: #{numOrig}/#{num}/#{mesta[specID]}"
end

commitDBs()


