require_relative 'pkutils'
require 'nokogiri'

token = "En4bHRXtJSYamzT9Q0V2kOpd7nfjJxe3"


mainUrl="https://prkomm.ugatu.su/abitur/ratelist/spoRate/"

hdrs={ "Upgrade-Insecure-Requests":"1",
 "Cache-Control":"max-age=0",
 "Cookie":"csrftoken="+token,
 "Origin":"https://prkomm.ugatu.su",
 "Referer":mainUrl,
}

Dir.mkdir('ugatu_tech') unless Dir.exist?('ugatu_tech') 

if File.exist?('ugatu_tech/specs.htm')
  q = IO.read('ugatu_tech/specs.htm')
else
  q = httpToFile(mainUrl,'ugatu_tech/specs.htm')
end
doc = Nokogiri::HTML(q)

s = doc.xpath('//select[@name="specValue"]/option')


allspecs=[]
s.each do |opt|
  v = opt.attr('value')
  next if v.empty?
  allspecs.push(v)
  puts v
end
  
allspecs.each do |spec|

parameters =  {
"unit":"Головной ВУЗ",
"edform":"Очная",
"EducationLevel":"СПО",
"specValue":spec,
"doc":"Все",
"docOsn":"Бюджет",
"comment":"Все",
"csrfmiddlewaretoken":token
}


s = httpToString(mainUrl, post:true, headers:hdrs, params:parameters, debug:true)

#<h4><span><div id="ratelist" style="text-align:center;"><strong>Конкурсные списки по направлению подготовки<br> 09.02.05 Прикладная информатика (по отраслям) (Бюджет) <br>по состоянию на 15.07.2017 20:30:02</strong></div> </span></h4><br>
#<p><div style="text-align:center;"><span><strong>Мест вакнтно - 50 (план приема - 50, зачислено - 0, отчислено - 0)</strong></span></div></p>
#<br>
#<div style="overflow-x:scroll;width:100%">
#<br>
#<table class="table table-bordered" style="width:100%;font-size:80%">


s1 = /<div style=\"overflow-x.*?<\/div>/m.match(s)
s2 = /<h4><span>.*?<\/p>/m.match(s)

if s1.nil? or  s2.nil? 
  printf "Не найден список: spec=%s\n", spec
  next
end

specName = /<br>\s+(\d\d\.\d\d\.\d\d.*?) <br>/m.match(s2[0])

dt = /по состоянию на (\d+)\.(\d+)\.(\d+)\s(\d+)/.match(s2[0])
plan = /(план приема|вакантные места) - (\d+)/.match(s2[0])

printf "%s %s %s\n", spec,dt[1],specName[1]

zz= s1[0].scan(/<td>Оригинал<\/td>[^<]*<td>[^>]*<\/td>[^<]*<\/tr>/)
origNum = zz.size()
zz= s1[0].scan(/<td>Копия<\/td>[^<]*<td>[^>]*<\/td>[^<]*<\/tr>/)
copyNum = zz.size()
totalNum = origNum+copyNum


fname=getFileName('ugatu_tech',specName[1],"#{dt[3]}.#{dt[2]}.#{dt[1]}.#{dt[4]}")

File.open(fname, 'w') {|f| 
f.write "<html><head><title>"+origNum.to_s+'/'+plan[2]+' '+specName[1]+"</title>"
f.write "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
f.write "</head><body>"
f.write "<h4> Всего "+totalNum.to_s+", оригиналов "+ origNum.to_s+", копий "+copyNum.to_s+"</h4>"
f.write s2
f.write "\n"
s=s1[0]
fixE(s)
f.write s
f.write "</body></html>\n"

}


end



