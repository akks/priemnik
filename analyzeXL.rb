﻿require_relative 'mod_usatu'
require 'set'
require 'rubyXL'

@magistr=false if @magistr.nil?


wb = RubyXL::Workbook.new

if @magistr


files = Dir.glob("magistr/*.html") 
stats = File.open('stats_magistr.csv', 'w:cp1251') 
konkurs = File.open('konkurs_magistr.csv', 'w:cp1251')
statsO = File.open('stats_orig_magistr.csv', 'w:cp1251') 
konkursO = File.open('konkurs_orig_magistr.csv', 'w:cp1251')

else

files = Dir.glob("bakalavr/*.html") 

wb.add_worksheet('Stats')
wb.add_worksheet('StatsO')
wb.add_worksheet('Konkurs')
wb.add_worksheet('KonkursO')
stats = wb['Stats']
konkurs =  wb['Konkurs']
statsO =  wb['StatsO']
konkursO =  wb['KonkursO']
wb.worksheets.delete_at(0)
end

counts=Hash.new
konkursVals=Hash.new
countsO=Hash.new
konkursValsO=Hash.new

dates=SortedSet.new
specs=SortedSet.new

plans=Hash.new
cels=Hash.new

specFullName=Hash.new

total=Hash.new
totalO=Hash.new

totalGr=Hash.new
totalGrO=Hash.new

p = UGATUParser.new(@magistr)

for f in files do

	print "File #{f} \n" 

	p.readHeaders(f)

	p.findSpec() do |specID, spec, totalPlan, celPlan, dateStr|
		dates.add(dateStr)
		specs.add(spec)
		specFullName[specID]=spec
		plans[spec] = totalPlan
		cels[spec] = celPlan
		
		totalNum = 0
		origNum = 0
		p.forAllRecords() do |num,name,ball,b1,b3,b2,orig|
			totalNum += 1
			origNum += 1 if orig
		end
		
		puts "#{dateStr} #{spec} #{totalNum}"
		
		counts[[dateStr,spec]]=totalNum
		konkursVals[[dateStr,spec]]='%.2f'% (1.0*totalNum/totalPlan)

		countsO[[dateStr,spec]]=origNum
		konkursValsO[[dateStr,spec]]='%.2f'% (1.0*origNum/totalPlan)
	end

end


i=0

[stats, statsO, konkurs, konkursO].each{ |ws|
	ws.add_cell(i,0,"специальность")
	ws.add_cell(i,1,"м")
	ws.add_cell(i,2,"ц")
j=2
dates.each { |date|
#	stats.write ";#{date}"
	ws.add_cell(i,j,"#{date}")
	ws.change_column_width(j,4)
	j+=1     
	total[date] = 0
	totalO[date] = 0
}
}
wb.save("stats.xlsx")

groups={'ОНФ'=>['01.03.02','01.03.04','02.03.01'], 'ФИРТ-И'=>['02.03.03','09.03.01','09.03.02','09.03.03','09.03.04','09.05.01','10.03.01','10.05.05']}

groups.values.each { |v|
 dates.each { |date|
   totalGr[[v,date]] = 0
   totalGrO[[v,date]] = 0
 }
}

specs.each { |spec|
	i+=1
	[stats, statsO, konkurs, konkursO].each{ |ws|
	j=0
	ws.add_cell(i,j,"#{spec}")
	j+=1
	ws.add_cell(i,j,plans[spec])
	j+=1
	ws.add_cell(i,j,cels[spec])
	}
	j=2	
	dates.each { |date|
		v = counts[[date,spec]]
		v='0' if v.nil? 
		stats.add_cell(i,j,v.to_i)

		total[date] += v.to_i
	
		v = konkursVals[[date,spec]]
		v='0.0' if v.nil? 
		v['.']=','
		konkurs.add_cell(i,j,v)
		
		v = countsO[[date,spec]]
		v='0' if v.nil? 
		totalO[date] += v.to_i
                statsO.add_cell(i,j,v.to_i)
		
		v = konkursValsO[[date,spec]]
		v='0.0' if v.nil? 
		v['.']=','
		konkursO.add_cell(i,j,v)
		j+=1
	}
}
j=2
i+=1
dates.each { |date|
        stats.add_cell(i,j,total[date].to_i)
        statsO.add_cell(i,j,totalO[date].to_i)
	j+=1
}
groups.each { |gr,spcs|
j=1
i+=1
stats.add_cell(i,j,gr)
statsO.add_cell(i,j,gr)
j+=1

dates.each { |date|
 totalGr[[date,gr]] = 0
 totalGrO[[date,gr]] = 0
 spcs.each { |specID|
   spec = specFullName[specID]
   totalGr[[date,gr]] += counts[[date,spec]].to_i
   totalGrO[[date,gr]] += countsO[[date,spec]].to_i
 }
 stats.add_cell(i,j,totalGr[[date,gr]].to_i)
 statsO.add_cell(i,j,totalGr[[date,gr]].to_i)
 j+=1
}
}

wb.save("stats.xlsx")