require 'nokogiri'
require_relative 'pkdb'

@mag=false if @mag.nil?

@token = 'ORFBw2Z5qzFNTztPd36FYDIRU14hzU2xVhEhc5ujmK6kH6hapm10xdseoz9R3HP8'

@mainUrl='https://www.ugatu.su/abitur/info/bach/#ratelist'

db=PKDB.new('places.db')
db.createTables()
db.prepare()

if true
  parameters = {
    'unit' => 'Головной ВУЗ',
    'edform' => 'Очная',
    'docOsn' => 'Бюджет',
    'csrfmiddlewaretoken' => @token
  }

  hdrs={ 'Upgrade-Insecure-Requests' => '1',
    'Accept' => 'text/html',
    'Cache-Control' => 'max-age=0',
    'Cookie' => 'csrftoken='+@token,
    'Origin' => 'https://ugatu.su',
    'Referer' => @mainUrl,
  }
  
  path = 'counts.html'
  if false #File.exist?(path)
    f = File.open(path)
    q= IO.read(f)
    q.force_encoding('UTF-8')
  else
    q = httpToFile(@mainUrl, path, post:true, params:parameters, headers:hdrs)
  end



  doc = Nokogiri::HTML(q)
  
  m = q.match(/по состоянию на (\d+)\.(\d+)\.(\d+)\s(\d+):(\d+):(\d+)/)
  h=m[4].to_s
  h='09' if h=='9'
  dt= "#{m[2]}.#{m[1]}.#{h}.#{m[5]}"

  s = doc.xpath('//table/tr')
  s.each do |v|
    cc = v.children
    q= cc[1].children[0]
    z= q.children[8]
    next if z.nil?
    if z
      s = z['value']
    else
      s=''
    end
    next if s.nil?
   # puts s
    #c = cc[5].text.split('/')
    #l = cc[7].text.split('/')
    #o = cc[8].text.split('/')
    
    o = cc[6].text.split('/')[0].to_i
    z = cc[3].text.to_i-cc[4].text.to_i
    mest = cc[5].text.to_i


    puts "'#{s[/(\d+\.\d+\.\d+)/,1]}'=>#{(mest*0.8).ceil+z},"
    
    db.addOrigs(s,dt,o+z)
    #puts [s,c[0],l[0],o[0]].to_s
  end

  db.commit()
end


fname="counts2.html"

f = File.open(fname, 'w')
print "Сохраняем в файл #{fname}: "

f.write "<html><head><title>Количество заявлений (история), обновление ращз в 10 минут</title>"

f.puts """
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<style type='text/css'>
table { 
  border:solid; 
  width:100%;
  font-size:100%
  }
  table tr {
    background-color: lightgreen;
    vertical-align: top;
  }
  table tr td:n-th-child(4) {
    word-break:break-word;
  }
  </style>
  </head>
  <body>
  Количества по состоянию на #{dt}
  <table>
"""


db1 = PKDB.new('data_all.db')
db1.createTables()
db1.prepare(false)

specs = Hash.new
mesta = Hash.new

db1.execQuery("SELECT spec,vuz,specName,mest FROM specs where vuz='УГАТУ'").each {|row|
  sn = row[2].gsub(/\d\d\.\d\d\.\d\d[^$:]/,'')
  specs[row[2]]="<a href='#{row[0]}.html'>#{sn}</a>"
  mesta[row[2]]=row[3]
}

puts mesta



groups = db.execQuery("SELECT spec,dt, origs FROM timePlaces order by spec, dt").group_by {|r| r[0]}

groups.each do |key,vals|
  next if key.nil? || key==''
  op = 0
 # puts key
  n = vals.count
  k=0
  list=''
  omax=0
  vals.each do |v|
    k=k+1
    o =  v[2].to_i
    omax=o
    if op!=o 
      list+=', ' if list!=''
      list+= "#{v[1][6..-1]}: <b>#{o}</b>"
      op=o
    end
  end


  f.puts "<tr><td>#{specs[key]}</td><td>#{mesta[key]}</td><td><b>#{omax}</b></td><td>#{list}</td></tr>"
end

f.puts """
  </table>
  </body>
  </html>
"""
