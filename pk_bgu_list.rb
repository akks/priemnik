﻿require_relative 'pkutils'
require 'watir'
require_relative 'pkhead'
require 'nokogiri'

MaybeHeadless do

profile = Selenium::WebDriver::Firefox::Profile.new('/home/user/.mozilla/firefox/1o16j5in.default')
b = Watir::Browser.new :firefox, :profile => profile, "acceptInsecureCerts" => true

specs=Set.new
b.goto 'https://priem.bashedu.ru/list'

Nokogiri::HTML(b.html).xpath("//select[@id='sp']/option").each { |opt|
#b.select(:id => 'sp').options.each { |opt|
spopt = opt['value']
specs.add(spopt)
}

dt = Time.now.strftime("%Y.%m.%d.%H")
i=0
n=specs.count
for spec in specs 
#url = "https://priem.bashedu.ru/rating?el=2&em=11&sp=#{spec}&uz=01&dor=3&sof=1&apply=true"
url = "https://priem.bashedu.ru/list?el=2&em=11&uz=01&sof=1&sp=#{spec}&dor=3&sofr=1&apply=true"
b.goto url

filename = getFileName("bsu",spec,dt)
puts "Saving #{url} to #{filename}, #{100*i/n}%"
File.open(filename, 'wb') {|f| 
  f.write b.html
}
i=i+1
end



end
